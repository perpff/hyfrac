# Repo for 'hyfrac'

This repo was moved from bitbucked in an effort to have everything in a central place.

## Build
This *should* work with all versions of QT 4+x.
- make sure qconf (or qmake) is installed on your system.
- change to 'sources' directory
- `qmake`
- `make`

Hyfrac is a finite difference software, intended to model fluid flow through porous rocks and through fractures in crustal rocks. Fractures form (and heal) as fluid pressure exceeds solid pressure. Flow is modeled as Darcian flow and is able to simulate the formation of fracture networks, reservoirs, propagation of individual fractures, mobile hydro-fractures.


![Fracture pattern](fracture_pattern_hyfracs.png)

*Simple hydraulic fracture system modeled with hyfrac*

### Relevant publication

Sachau, Till, Paul D. Bons, und Enrique Gomez-Rivas. „Transport Efficiency and Dynamics of Hydraulic Fracture Networks“. Frontiers in Physics 3 (21. August 2015). https://doi.org/10.3389/fphy.2015.00063.