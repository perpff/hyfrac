#include "basicsettings.h"

BasicSettings::BasicSettings()
{
    // default settings
    dimx = 100;
    dimy = 200;
    total_timesteps = 10000;
    total_time = 1000.0 * a;

    length_in_x = 8000;
    top_layer_depth = 0000.0;

    rate_of_subsidence = 0.0 / a;

    fracture_conductivity = 0.01;
    fracture_viscosity = 1e19;
    fixed_fracture_viscosity = true;
    viscoelastic = false; // if false: viscous closing of fractures

	// the shape factor below works like that (in the 'Seal'-function in model.functions.cpp):
	// tau = viscosity / nd->young;
	// tau = tau * basic_settings.shape_factor_fracture_closure;
    shape_factor_fracture_closure = 1.0;

    elastic_only_closure = false;           // this _should_ be false
    memory_factor_tensile_strength = 1.0;	// regarding initial tensile strength, no effect = 1.0
    memory_factor_conductivity = 0.0;		// regarding fracture conductivity, no effect = 0.0

    // is there a damage zone in front of the fracture?
    damage_zone = false;

    // does the fracture itself use damage conductivity?
    damage_conductivity = false;

    fracture_porosity = 0.28;

    fixed_fracture_conductivity = true;
    use_cubic_law = false;

    seepage = true;

    rock_viscosity_A = 8.92e27;
    rock_viscosity_E = 1.39e5;
    rock_viscosity_n = 3.4;
    rock_viscosity_V = 1e-5;

    // a value between 0 (no prefracking at all) and 1 (absolute prefracking)
    fracking_prefactor = 0.0;

    // save modelfile every 10 timesteps
    SaveInterval = 10;

    aperture_max_show = 100;
    aperture_min_show = 0;

    ScaleMax = ScaleMin = 0.0;

    save_png = false;

    count_fractures_to_save_file = false;
    continuous_signal = false;
}
