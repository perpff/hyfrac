#ifndef BASICSETTINGS_H
#define BASICSETTINGS_H

#include "definitions.h"

class BasicSettings
{
public:
    BasicSettings();

    int dimx;
    int dimy;
    int total_timesteps;
    REAL total_time;
    
    bool viscoelastic; 	// if false: visous closing of fractures

    bool elastic_only_closure;
    bool damage_zone;
    REAL memory_factor_tensile_strength;	// regarding initial tensile strength
    REAL memory_factor_conductivity;		// regarding fracture conductivity

    REAL length_in_x;
    REAL top_layer_depth;
    
    REAL shape_factor_fracture_closure;
    
    REAL rate_of_subsidence;

    bool count_fractures_to_save_file;
    bool continuous_signal;
    
    REAL rock_viscosity_A;
    REAL rock_viscosity_E;
    REAL rock_viscosity_n;
    REAL rock_viscosity_V;
    
    REAL fracking_prefactor;
    
    REAL fracture_conductivity;
    REAL fracture_porosity;
    REAL fracture_viscosity;
    bool damage_conductivity;
    
    bool seepage;

    bool fixed_fracture_viscosity;
    bool fixed_fracture_conductivity;
    bool use_cubic_law;
    
    int SaveInterval;
    int aperture_min_show, aperture_max_show;
    bool save_png;
	
    REAL ScaleMax, ScaleMin;
};

#endif // BASICSETTINGS_H
