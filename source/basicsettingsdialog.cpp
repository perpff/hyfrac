#include "basicsettingsdialog.h"
#include "ui_basicsettingsdialog.h"

#include <iostream>

BasicSettingsDialog::BasicSettingsDialog(MainWindow *mainwindow, BasicSettings *basic_settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BasicSettingsDialog)
{
    ui->setupUi(this);

    BasicSettingsDialog::mainwindow = mainwindow;

    BasicSettingsDialog::basic_settings = basic_settings;
    BasicSettingsDialog::parent = parent;

    reload_dialog();

}

BasicSettingsDialog::~BasicSettingsDialog()
{
    delete ui;
}

void BasicSettingsDialog::on_buttonBox_accepted()
{
    basic_settings->ScaleMax = ui->doubleSpinBox_scale_bar_max->value();
    basic_settings->ScaleMin = ui->doubleSpinBox_scale_bar_min->value();

    mainwindow->SettingsChanged();
    mainwindow->repaint();
}


void BasicSettingsDialog::on_pushButton_clicked()
{
    on_buttonBox_accepted();
}

void BasicSettingsDialog::reload_dialog()
{
    ui->doubleSpinBox_scale_bar_max->setMaximum(1e20);
    ui->doubleSpinBox_scale_bar_max->setMinimum(-(1e20));
    ui->doubleSpinBox_scale_bar_max->setValue(basic_settings->ScaleMax);

    ui->doubleSpinBox_scale_bar_min->setMaximum(1e20);
    ui->doubleSpinBox_scale_bar_min->setMinimum(-(1e20));
    ui->doubleSpinBox_scale_bar_min->setValue(basic_settings->ScaleMin);
}


void BasicSettingsDialog::on_pushButton_2_clicked()
{
    reload_dialog();
}

void BasicSettingsDialog::on_horizontalSlider_aperture_min_valueChanged(int value)
{
    basic_settings->aperture_min_show = value;

    if (value > basic_settings->aperture_max_show)
    {
        basic_settings->aperture_max_show = value;

        ui->horizontalSlider_aperture_max->setSliderPosition(value);
    }

    mainwindow->ChangeApertureRange();
}

void BasicSettingsDialog::on_horizontalSlider_aperture_max_valueChanged(int value)
{
    basic_settings->aperture_max_show = value;

    if (value < basic_settings->aperture_min_show)
    {
        basic_settings->aperture_min_show = value;

        ui->horizontalSlider_aperture_min->setSliderPosition(value);
    }

    mainwindow->ChangeApertureRange();
}
