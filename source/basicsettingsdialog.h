#ifndef BASICSETTINGSDIALOG_H
#define BASICSETTINGSDIALOG_H

#include <QDialog>
#include "basicsettings.h"
#include "mainwindow.h"

class MainWindow;

namespace Ui {
class BasicSettingsDialog;
}

class BasicSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BasicSettingsDialog(MainWindow *mainwindow = 0, BasicSettings *basic_settings = 0, QWidget *parent = 0);
    ~BasicSettingsDialog();

private slots:
    void on_buttonBox_accepted();
    void on_pushButton_clicked();
    void reload_dialog();

    void on_pushButton_2_clicked();

    void on_horizontalSlider_aperture_min_valueChanged(int value);

    void on_horizontalSlider_aperture_max_valueChanged(int value);

private:
    Ui::BasicSettingsDialog *ui;
    BasicSettings *basic_settings;
    MainWindow *mainwindow;
    QWidget *parent;
};

#endif // BASICSETTINGSDIALOG_H
