#!/usr/bin/python

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 12:13:55 2015

@author: till
"""

from scipy import signal
from pylab import *

data = np.loadtxt("fracture_ratio.csv",  skiprows=1)
timesteps, fractured_nodes,total_nodes = data[:,0], data[:,3], data[:,4]
spacing = 0.01

print ("Spacing: ", spacing)
print ("Spacing is 1.0 / nb_of_nodes_on_the_larger_side")
print ("PLEASE check if the ratio of the length in x / length in y is correct!!!")

dim = log(fractured_nodes) / log(1.0/spacing)

clf()
plot(timesteps, dim)
show(block=True)
