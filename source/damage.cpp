#include "damage.h"

Damage::Damage()
{
    t_f = 0;			// time to failure

    v_0 = 0;			// reference hazard rate
    s_0 = 0;			// reference stress

    rho = 3;			// power-law exponent for the damage model

    v = 0;				// hazard rate
}

Damage::~Damage()
{
}

REAL Damage::CalcFailureTime()
{
    // failure of a single element in a grid (Yakovlev et al, 2010)
    // t_f = 1.0/v_0 * log(1.0/(1.0 - P_c));

    // failure of the grid
}
