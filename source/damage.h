#ifndef DAMAGE_H
#define DAMAGE_H

#include <iostream>
#include <math.h>
#include "definitions.h"

class Damage
{
public:
    Damage();
    ~Damage();

    REAL CalcFailureTime();

private:
    REAL t_f;			// time to failure
    REAL v_0;			// reference hazard rate
    REAL s_0;			// reference stress
    REAL rho;			// power-law exponent for the damage model
    REAL v;				// hazard rate
};

#endif // DAMAGE_H
