/*
 * =====================================================================================
 *
 *       Filename:  definitions.h
 *
 *    Description:  basic definitions, typedefs, constants, ...
 *
 *        Version:  1.0
 *        Created:  24.07.2014 22:46:25
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

typedef double REAL;

static const REAL g = 9.81;						// gravitational acceleration in m/s^2
static const REAL pi = 3.14159265358979323846;	// pi

static const REAL av_crustal_density = 2800.0;	// average density of crustal rock in kg/m^3
//static const REAL E_Young = 5e10;				// from enrique, not sure what he used it for
static const REAL E_Young = 80e9;
static const REAL crustal_poisson_ratio = 0.3;

static const REAL water_density = 1000.0;		// density of water in kg/m^3
static const REAL water_compressibility_base =  5.1e-10;	// compressibility of water in 1/Pa
static const REAL water_bulk_modulus = 2.5e9;				// at 20 ° (I think)
static const REAL water_viscosity = 8.9e-4; 		// Pa·s

static const REAL gas_constant = 8.31446;		// J/(K*mol);

static const REAL a = 365 * 86400;				// year in seconds

#endif

