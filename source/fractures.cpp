#include "fractures.h"

Fractures::Fractures(Model *Mod)
{
    nb_of_fractures = 0;
    M = Mod;
}

void Fractures::UpdateFractureList()
{
    Node *nd;

    FractureList.clear();
    nb_of_fractures = 0;
    for (int i=0; i<M->dimx; i++)
    {
        for (int j=0; j<M->dimy; j++)
        {
            M->Nodes[i][j].fracture_number = -1;
        }
    }

    for (int i=0; i<M->dimx; i++)
    {
        for (int j=0; j<M->dimy; j++)
        {
            nd = &(M->Nodes[i][j]);

            if (nd->fracture)
            {
                if (nd->fracture_number < 0)
                {
                    nd->fracture_number = nb_of_fractures++;
                    FractureList.push_back(vector < Node* > () );
                    FractureList[nd->fracture_number].push_back(nd);
                    AddConnectedNeighbours(nd);
                }
            }
        }
    }
}


void Fractures::AddConnectedNeighbours(Node* nd)
{
    if (nd->broken_in_x)
    {
        if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0))
        {
            if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0)->broken_in_x)
            {
                if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0)->fracture_number < 0)
                {
                    M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0)->fracture_number = nd->fracture_number;
                    FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0));
                    AddConnectedNeighbours(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 0));
                }
            }
        }

        if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2))
        {
            if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2)->broken_in_x)
            {
                if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2)->fracture_number < 0)
                {
                    M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2)->fracture_number = nd->fracture_number;
                    FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2));
                    AddConnectedNeighbours(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 2));
                }
            }
        }
    }

    if (nd->broken_in_y)
    {
        if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1))
        {
            if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1)->broken_in_y)
            {
                if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1)->fracture_number < 0)
                {
                    M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1)->fracture_number = nd->fracture_number;
                    FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1));
                    AddConnectedNeighbours(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 1));
                }
            }
        }

        if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3))
        {
            if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3)->broken_in_y)
            {
                if (M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3)->fracture_number < 0)
                {
                    M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3)->fracture_number = nd->fracture_number;
                    FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3));
                    AddConnectedNeighbours(M->GetNodeNeighbour(nd->row_index*M->dimx + nd->col_index, 3));
                }
            }
        }
    }
}

void Fractures::CalculateFracturePressure()
{
    REAL total_fracture_volume;

    // loop through fractures
    for (int i=0; i<FractureList.size(); i++)
    {
        for (int j=0; j<FractureList[i].size(); j++)
        {

            ; //cout << FractureList[i][j]->row_index << endl;
        }
    }
}

/* the old version */
//void Fractures::UpdateFractureList()
//{
//    Node *nd;

//    bool nb_exists = false;

//    FractureList.clear();
//    nb_of_fractures = 0;
//    for (int i=0; i<M->dimx; i++)
//    {
//        for (int j=0; j<M->dimy; j++)
//        {
//            M->Nodes[i][j].fracture_number = -1;
//        }
//    }

//    for (int i=0; i<M->dimx; i++)
//    {
//        for (int j=0; j<M->dimy; j++)
//        {

//            nd = &(M->Nodes[i][j]);
//            nb_exists = false;

//            if (nd->fracture)
//            {
//                if (nd->fracture_number >= 0) // look for neighbor fractures, attach them to the array if necessary
//                {
//                    nb_exists = true;

//                    if (nd->broken_in_x)
//                    {
//                        if (M->GetNodeNeighbour(j*M->dimx + i, 0)->broken_in_x)
//                        {
//                            FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(j*M->dimx + i, 0));
//                            M->GetNodeNeighbour(j*M->dimx + i, 0)->fracture_number = nd->fracture_number;
//                        }

//                        if (M->GetNodeNeighbour(j*M->dimx + i, 2)->broken_in_x)
//                        {
//                            FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(j*M->dimx + i, 2));
//                            M->GetNodeNeighbour(j*M->dimx + i, 2)->fracture_number = nd->fracture_number;
//                        }
//                    }

//                    if (nd->broken_in_y)
//                    {
//                        if (M->GetNodeNeighbour(j*M->dimx + i, 1)->broken_in_y)
//                        {
//                            FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(j*M->dimx + i, 1));
//                            M->GetNodeNeighbour(j*M->dimx + i, 1)->fracture_number = nd->fracture_number;
//                        }

//                        if (M->GetNodeNeighbour(j*M->dimx + i, 3)->broken_in_y)
//                        {
//                            FractureList[nd->fracture_number].push_back(M->GetNodeNeighbour(j*M->dimx + i, 3));
//                            M->GetNodeNeighbour(j*M->dimx + i, 3)->fracture_number = nd->fracture_number;
//                        }
//                    }
//                }

//                else // check if node belongs to a neighbor fracture, and add it if necessary
//                {
//                    if (nd->broken_in_x)
//                    {

//                        if (M->GetNodeNeighbour(j*M->dimx + i, 0)->broken_in_x)
//                        {
//                            if (M->GetNodeNeighbour(j*M->dimx + i, 0)->fracture_number >= 0)
//                            {
//                                nd->fracture_number = M->GetNodeNeighbour(j*M->dimx + i, 0)->fracture_number;
//                                cout << M->GetNodeNeighbour(j*M->dimx + i, 0)->fracture_number << endl;
//                                FractureList[M->GetNodeNeighbour(j*M->dimx + i, 0)->fracture_number].push_back(nd);
//                                nb_exists = true;
//                            }
//                        }

//                        if (M->GetNodeNeighbour(j*M->dimx + i, 2)->broken_in_x)
//                        {
//                            if (M->GetNodeNeighbour(j*M->dimx + i, 2)->fracture_number >= 0)
//                            {
//                                nd->fracture_number = M->GetNodeNeighbour(j*M->dimx + i, 2)->fracture_number;
//                                FractureList[M->GetNodeNeighbour(j*M->dimx + i, 2)->fracture_number].push_back(nd);
//                                nb_exists = true;
//                            }
//                        }

//                    }

//                    if (nd->broken_in_y)
//                    {
//                        if (M->GetNodeNeighbour(j*M->dimx + i, 1)->broken_in_y)
//                        {
//                            if (M->GetNodeNeighbour(j*M->dimx + i, 1)->fracture_number >= 0)
//                            {
//                                nd->fracture_number = M->GetNodeNeighbour(j*M->dimx + i, 1)->fracture_number;
//                                FractureList[M->GetNodeNeighbour(j*M->dimx + i, 1)->fracture_number].push_back(nd);
//                                nb_exists = true;
//                            }
//                        }

//                        if (M->GetNodeNeighbour(j*M->dimx + i, 3)->broken_in_y)
//                        {
//                            if (M->GetNodeNeighbour(j*M->dimx + i, 3)->fracture_number >= 0)
//                            {
//                                nd->fracture_number = M->GetNodeNeighbour(j*M->dimx + i, 3)->fracture_number;
//                                FractureList[M->GetNodeNeighbour(j*M->dimx + i, 3)->fracture_number].push_back(nd);
//                                nb_exists = true;
//                            }
//                        }
//                    }

//                    if (!nb_exists)
//                    {
//                        nd->fracture_number = nb_of_fractures++;
//                        FractureList.push_back(vector < Node* > () );
//                        FractureList[nd->fracture_number].push_back(nd);
//                    }
//                }
//            }
//        }
//    }
//}
