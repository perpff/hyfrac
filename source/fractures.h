#ifndef FRACTURES_H
#define FRACTURES_H

#include "node.h"
#include "model.h"

#include <list>
#include <vector>

using namespace std;

class Model;

class Fractures
{

public:
    Fractures(Model *Mod);
    void UpdateFractureList();
    void CalculateFracturePressure();

private:
    void AddConnectedNeighbours(Node* nd);

    Model *M;
    vector < vector < Node* > > FractureList;

    int nb_of_fractures;
};

#endif // FRACTURES_H
