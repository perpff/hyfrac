QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = hyfrac
TEMPLATE = app

SOURCES += main.cpp\
		   mainwindow.cpp \
		   qcustomplot.cpp \
    model.cpp \
    node.cpp \
    model.solution.cpp \
    model.simulation.cpp \
    basicsettingsdialog.cpp \
    statusdialog.cpp \
    damage.cpp \
    basicsettings.cpp \
    statevariables.cpp \
    model.functions.cpp \
    model.new_faulting.cpp \
    fractures.cpp

HEADERS += mainwindow.h \
		   qcustomplot.h \
    model.h \
    node.h \
    definitions.h \
    basicsettingsdialog.h \
    statusdialog.h \
    damage.h \
    basicsettings.h \
    statevariables.h \
    fractures.h

FORMS	+= mainwindow.ui \
    basicsettingsdialog.ui \
    statusdialog.ui

# openMP
QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp
