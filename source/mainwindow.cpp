#include "mainwindow.h"

#include <QDebug>
#include <QDesktopWidget>
#include <QScreen>
#include <QMessageBox>
#include <QMetaEnum>

#include <iostream>


MainWindow::MainWindow(Model *M, int argc, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    display = ui->comboBox->currentText();

    MainWindow::M = M;
    SetupPlot(argc);

    pause = false;
    autorescale = false;

    // set the variables
    on_checkBox_count_fractures_stateChanged();
}

void MainWindow::on_checkBox_count_fractures_stateChanged()
{
    M->basic_settings.count_fractures_to_save_file = ui->checkBox_count_fractures->isChecked();
}

void MainWindow::on_checkBox_continuous_signals_stateChanged()
{
        M->basic_settings.continuous_signal = ui->checkBox_continuous_signals->isChecked();
}

void MainWindow::SetupPlot(int arg)
{
    setupColorMap(ui->customPlot);

    if (arg == 1)
    {
        WindowTitle("Flow model");
    }
    else
    {
        WindowTitle("View model files");
        enable_pushButton_start(false);
    }

    statusBar()->clearMessage();
}

void MainWindow::WindowTitle(QString title)
{
    setWindowTitle(title);
}

void MainWindow::Replot()
{

    colorMap->data()->setRange(QCPRange(0.0, M->length_in_x), QCPRange(M->GetTopLayerPosition(), M->GetBottomLayerPosition())); 
    colorMap->data()->setSize(M->dimx, M->dimy); // we want the color map to have nx * ny data points

    REAL buffer;
    REAL tmin, tmax;


    if (display == "Conductivity in X")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetConductivityInX(xIndex, yIndex));
            }
        }
    }
    else if (display == "Tensile strength X")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetTensileStrengthX(xIndex, yIndex));
            }
        }
    }
    else if (display == "Overpressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetOverPressure(xIndex, yIndex));
            }
        }
    }
    else if (display == "Tensile strength Y")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetTensileStrengthY(xIndex, yIndex));
            }
        }
    }
    else if (display == "x_damage")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetXDamage(xIndex, yIndex));
            }
        }
    }
    else if (display == "y_damage")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetYDamage(xIndex, yIndex));
            }
        }
    }
    else if (display == "Fluid mass")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFluidMass(xIndex, yIndex));
            }
        }
    }
    else if (display == "Active fractures")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFractureActivity(true, xIndex, yIndex));
            }
        }
    }
    else if (display == "Inactive fractures")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFractureActivity(false, xIndex, yIndex));
            }
        }
    }
    else if (display == "Broken in X")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetBrokenInX(xIndex, yIndex));
            }
        }
    }
    else if (display == "Broken in Y")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetBrokenInY(xIndex, yIndex));
            }
        }
    }
    else if (display == "Fracture number")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFractureNumber(xIndex, yIndex));
            }
        }
    }
    else if (display == "yaperture/spacing")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                buffer = M->GetYApertureRatio(xIndex, yIndex);

//                if (buffer > tmin && buffer < tmax)
                    colorMap->data()->setCell(xIndex, yIndex, M->GetYApertureRatio(xIndex, yIndex));
            }
        }
    }
    else if (display == "xaperture/spacing")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                buffer = M->GetXApertureRatio(xIndex, yIndex);

//                if (buffer > tmax && buffer < tmin)
                    colorMap->data()->setCell(xIndex, yIndex, M->GetXApertureRatio(xIndex, yIndex));
            }
        }
    }
    else if (display == "Conductivity in Y")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetConductivityInY(xIndex, yIndex));
            }
        }
    }
    else if (display == "Hydraulic head")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetHydraulicHead(xIndex, yIndex));
            }
        }
    }
    else if (display == "Fractures")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFractureStatus(xIndex, yIndex));
            }
        }
    }
    else if (display == "Fluid pressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetFluidPressure(xIndex, yIndex));
            }
        }
    }
    else if (display == "Solid pressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetSolidPressure(xIndex, yIndex));
            }
        }
    }
    else if (display == "Corrected fluid pressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetCorrectedFluidPressure(xIndex, yIndex));
            }
        }

    }
    else if (display == "Diffusivity in X")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetDiffusivityInX(xIndex, yIndex));
            }
        }
    }
    else if (display == "Diffusivity in Y")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetDiffusivityInY(xIndex, yIndex));
            }
        }
    }
    else if (display == "Effective pressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetEffectivePressure(xIndex, yIndex));
            }
        }
    }
    else if (display == "x-stress")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetSx(xIndex, yIndex));
            }
        }
    }
    else if (display == "y-stress")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetSy(xIndex, yIndex));
            }
        }
    }
    else if (display == "Viscosity")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetViscosity(xIndex, yIndex));
            }
        }
    }
    else if (display == "Porosity")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetPorosity(xIndex, yIndex));
            }
        }
    }
    else if (display == "Temperature")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetTemperature(xIndex, yIndex));
            }
        }
    }
    else if (display == "Total pressure")
    {
        for (int xIndex=0; xIndex<M->dimx; ++xIndex)
        {
            for (int yIndex=0; yIndex<M->dimy; ++yIndex)
            {
                colorMap->data()->setCell(xIndex, yIndex, M->GetTotalPressure(xIndex, yIndex));
            }
        }
    }

    QCoreApplication::processEvents();

    if (autorescale)
        colorMap->rescaleDataRange(true);

    ui->customPlot->replot();

    M->basic_settings.ScaleMax = colorMap->dataRange().upper;
    M->basic_settings.ScaleMin = colorMap->dataRange().lower;
}

void MainWindow::SetTimestepLabel(int ts)
{
    ui->label_timestep_number->setText(QString::number(ts) + " / " + QString::number(M->total_timesteps));
}

void MainWindow::SetTotaltimeLabel(REAL totaltime)
{
    ui->label_time_passed->setText(QString::number(totaltime));
}

void MainWindow::enable_pushButton_start(bool enable)
{
    ui->pushButton_start->setEnabled(enable);
}

void MainWindow::on_pushButton_start_clicked()
{
    if (ui->pushButton_start->text() == "Run")
    {
        ui->pushButton_start->setText("Pause");
        pause = false;
        M->RunMainLoop(this);
    }
    else
    {
        ui->pushButton_start->setText("Run");
        pause = true;
    }
}

void MainWindow::setText_pushButton_start(QString string)
{
    ui->pushButton_start->setText(string);
}

void MainWindow::on_pushButton_reset_colormap_clicked()
{
    colorMap->rescaleDataRange(true);
    ui->customPlot->replot();

    M->basic_settings.ScaleMax = colorMap->dataRange().upper;
    M->basic_settings.ScaleMin = colorMap->dataRange().lower;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupColorMap(QCustomPlot *customPlot)
{
    // configure axis rect:
    customPlot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom); // this will also allow rescaling the color scale by dragging/zooming
    customPlot->axisRect()->setupFullAxesBox();
    customPlot->xAxis->setLabel("x");
    customPlot->yAxis->setLabel("y");

    customPlot->setBackground(Qt::lightGray);

    // set up the QCPColorMap:
    colorMap = new QCPColorMap(customPlot->xAxis, customPlot->yAxis);
    customPlot->addPlottable(colorMap);

    int nx = M->dimx;
    int ny = M->dimy;

    colorMap->data()->setSize(nx, ny); // we want the color map to have nx * ny data points
    colorMap->data()->setRange(QCPRange(0, M->length_in_x), QCPRange(M->GetTopLayerPosition(), M->GetBottomLayerPosition())); // and span the coordinate range -4..4 in both key (x) and value (y) dimensions

    // now we assign some data, by accessing the QCPColorMapData instance of the color map:
    for (int xIndex=0; xIndex<M->dimx; ++xIndex)
    {
        for (int yIndex=0; yIndex<M->dimy; ++yIndex)
        {
            colorMap->data()->setCell(xIndex, yIndex, M->GetCorrectedFluidPressure(xIndex, yIndex));
        }
    }

    // add a color scale:
    QCPColorScale *colorScale = new QCPColorScale(customPlot);
    customPlot->plotLayout()->addElement(0, 1, colorScale); // add it to the right of the main axis rect
    colorScale->setType(QCPAxis::atRight); // scale shall be vertical bar with tick/axis labels right (actually atRight is already the default)
    colorMap->setColorScale(colorScale); // associate the color map with the color scale
    colorScale->axis()->setLabel("Pressure in Pa");

    // set the color gradient of the color map to one of the presets:
    colorMap->setGradient(QCPColorGradient::gpHot);
    // we could have also created a QCPColorGradient instance and added own colors to
    // the gradient, see the documentation of QCPColorGradient for what's possible.

    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMap->rescaleDataRange(true);
    M->basic_settings.ScaleMax = colorMap->dataRange().upper;
    M->basic_settings.ScaleMin = colorMap->dataRange().lower;

    // make sure the axis rect and color scale synchronize their bottom and top margins (so they line up):
    QCPMarginGroup *marginGroup = new QCPMarginGroup(customPlot);
    customPlot->axisRect()->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);
    colorScale->setMarginGroup(QCP::msBottom|QCP::msTop, marginGroup);

    // rescale the key (x) and value (y) axes so the whole color map is visible:
    customPlot->rescaleAxes();
}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    //display = ui->comboBox->currentText();
    display = arg1;
    Replot();
    // rescale the data dimension (color) such that all data points lie in the span visualized by the color gradient:
    colorMap->rescaleDataRange(true);
    ui->customPlot->replot();
}

void MainWindow::SetSaveInterval()
{
    ui->checkBox_count_fractures->setChecked(M->basic_settings.count_fractures_to_save_file);
    ui->spinBox_save_interval->setValue(M->basic_settings.SaveInterval);
}

void MainWindow::on_pushButton_save_png_clicked()
{
    save_png();
}

void MainWindow::save_png()
{
    QString timestep;
    int ts;

    ts = M->save_number;

    if (ts < 10)
        timestep = "00000" + QString::number(ts);
    else if (ts < 100)
        timestep = "0000" + QString::number(ts);
    else if (ts < 1000)
        timestep = "000" + QString::number(ts);
    else if (ts < 10000)
        timestep = "00" + QString::number(ts);
    else if (ts < 100000)
        timestep = "0" + QString::number(ts);

    ui->customPlot->savePng(ui->comboBox->currentText() + "_" + timestep + ".png");
}

void MainWindow::on_pushButton_basic_settings_clicked()
{
    BasicSettingsDialog *settingsDialog = new BasicSettingsDialog(this, &(M->basic_settings), this);

    settingsDialog->setModal(false);


    settingsDialog->show() ;
}

void MainWindow::on_pushButton_state_clicked()
{
    StatusDialog *statusDialog = new StatusDialog(&M->state_variables, this);
    statusDialog->show();
}

void MainWindow::on_pushButton_save_model_clicked()
{
    M->SaveModel();
}

void MainWindow::on_pushButton_open_clicked()
{
    M->OpenModel(this, QString(""));
}

void MainWindow::on_spinBox_save_interval_valueChanged(int arg1)
{
    M->ChangeSaveInterval(arg1);
}

void MainWindow::SettingsChanged()
{
    // colorMap->rescaleDataRange(true);
    colorMap->setDataRange(QCPRange(M->basic_settings.ScaleMax, M->basic_settings.ScaleMin));
    ui->customPlot->replot();

    qApp->processEvents();
}


void MainWindow::ChangeApertureRange()
{
    ui->customPlot->replot();
}

void MainWindow::on_checkBox_save_png_stateChanged(int arg1)
{
   M->basic_settings.save_png = ui->checkBox_save_png->isChecked();
}

bool MainWindow::removeDir(const QString & dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists(dirName)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = dir.rmdir(dirName);
    }
    return result;
}

void MainWindow::on_pushButton_make_movie_clicked()
{

    QString basedirectory = QDir::currentPath();
    QString attribute = display;
    QString tmp_dirname = "tmp_" + attribute + "/";
    QString fileName_start = QFileDialog::getOpenFileName(this, tr("Choose first file"), "", tr("Files (*.mod)"));
    QString working_directory = fileName_start;
    QString working_directory_tmp;
    working_directory = working_directory.remove(fileName_start.lastIndexOf("/"), fileName_start.size());
    QDir::setCurrent(working_directory);
    QString fileName_end = QFileDialog::getOpenFileName(this, tr("Open last file"), "", tr("Files (*.mod)"));

    if (fileName_start.isEmpty() || fileName_end.isEmpty())
        return;

    QString start_number = fileName_start.remove(fileName_start.lastIndexOf("."), 4);
    start_number = fileName_start.remove(0, fileName_start.lastIndexOf("_")+1);
    int start_number_int = start_number.toInt();

    QString end_number = fileName_end.remove(fileName_end.lastIndexOf("."), 4);
    end_number = fileName_end.remove(0, fileName_end.lastIndexOf("_")+1);
    int end_number_int = end_number.toInt();

    int buffer;

    if (end_number_int < start_number_int)
    {
        buffer = end_number_int;
        end_number_int = start_number_int;
        start_number_int = buffer;
    }

    working_directory.append("/");
    working_directory_tmp = working_directory;
    working_directory_tmp.append(tmp_dirname);
    QDir().mkdir(working_directory_tmp);

    QString filename;
    QString timestep;

    for (int ts=start_number_int; ts <= end_number_int; ts++)
    {
        timestep = QString::number(ts);

        filename = working_directory + "timestep_" + timestep + ".mod";

        if (QFileInfo::exists(filename))
        {
            M->OpenModel(this, filename);

            if (ts < 10)
                timestep = "00000" + QString::number(ts);
            else if (ts < 100)
                timestep = "0000" + QString::number(ts);
            else if (ts < 1000)
                timestep = "000" + QString::number(ts);
            else if (ts < 10000)
                timestep = "00" + QString::number(ts);
            else if (ts < 100000)
                timestep = "0" + QString::number(ts);

            filename = working_directory_tmp + "timestep_" + timestep + ".png";
            ui->customPlot->savePng(filename);
        }
   }

    QDir::setCurrent(working_directory_tmp);
    QString program = "/usr/bin/mencoder";
    filename = basedirectory + "/movie_" + attribute.toLower() + "_" + start_number + "_to_" + end_number + ".avi";
    filename.replace(" ", "_");
    filename.replace(":", "_");

	//std::cout << filename.toStdString() << std::endl;

    QStringList arguments;

    arguments << "mf://*.png" << "-o" << filename << "-mf" << "type=png" << "-ovc" << "copy" << "-oac" << "copy";
    // mf://*.png -mf type=png -ovc copy -oac copy -o output.avi

    QProcess *myProcess = new QProcess(this);
    myProcess->setReadChannel(QProcess::StandardOutput);
    myProcess->waitForFinished(-1);
    myProcess->start(program, arguments);
    
     while(myProcess->state() != QProcess::NotRunning)
         myProcess->waitForReadyRead();

	removeDir(working_directory_tmp);

    QMessageBox::information(this, "Information", "The movie has been created!");
}

void MainWindow::on_checkBox_autoscale_stateChanged(int arg1)
{
    autorescale = ui->checkBox_autoscale->isChecked();
}

void MainWindow::ResetRunButton()
{
    ui->pushButton_start->setText("Run");
}

void MainWindow::on_pushButton_open_next_clicked()
{
    QString filename = M->model_file;
    QString number;
    int counter = 0;

    if (!filename.isEmpty())
    {
        number = filename;

        number.remove(number.lastIndexOf("."), 4);
        number.remove(0, number.lastIndexOf("_")+1);
        int number_int = number.toInt();
        number_int++;

        if (QFileInfo::exists(filename))
        {

            filename.remove(filename.lastIndexOf("_")+1, filename.size());
            filename.append(QString::number(number_int) + ".mod");

            while (!QFileInfo::exists(filename))
            {
                if (counter++ == 20000)
                    break;
                number_int ++;
                filename.remove(filename.lastIndexOf("_")+1, filename.size());
                filename.append(QString::number(number_int) + ".mod");
            }

            if (QFileInfo::exists(filename))
                M->OpenModel(this, filename);
        }
    }
}

void MainWindow::on_pushButton_open_previous_clicked()
{

    QString filename = M->model_file;
    QString number;
    int counter = 0;

    if (!filename.isEmpty())
    {
        number = filename;

        number.remove(number.lastIndexOf("."), 4);
        number.remove(0, number.lastIndexOf("_")+1);
        int number_int = number.toInt();
        number_int--;

        if (QFileInfo::exists(filename))
        {

            filename.remove(filename.lastIndexOf("_")+1, filename.size());
            filename.append(QString::number(number_int) + ".mod");

            while (!QFileInfo::exists(filename))
            {
                if (counter++ == 20000 || number_int < 0)
                    break;
                number_int --;
                filename.remove(filename.lastIndexOf("_")+1, filename.size());
                filename.append(QString::number(number_int) + ".mod");
            }

            if (QFileInfo::exists(filename))
                M->OpenModel(this, filename);
        }
    }
}

