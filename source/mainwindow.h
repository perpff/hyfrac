/***************************************************************************
**                                                                        **
**  QCustomPlot, an easy to use, modern plotting widget for Qt            **
**  Copyright (C) 2011, 2012, 2013, 2014 Emanuel Eichhammer               **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************
**           Author: Emanuel Eichhammer                                   ** 
**  Website/Contact: http://www.qcustomplot.com/                          **
**             Date: 07.04.14                                             **
**          Version: 1.2.1                                                **
****************************************************************************/

/************************************************************************************************************
**                                                                                                         **
**  This is the example code for QCustomPlot.                                                              **
**                                                                                                         **
**  It demonstrates basic and some advanced capabilities of the widget. The interesting code is inside     **
**  the "setup(...)Demo" functions of MainWindow.                                                          **
**                                                                                                         **
**  In order to see a demo in action, call the respective "setup(...)Demo" function inside the             **
**  MainWindow constructor. Alternatively you may call setupDemo(i) where i is the index of the demo       **
**  you want (for those, see MainWindow constructor comments). All other functions here are merely a       **
**  way to easily create screenshots of all demos for the website. I.e. a timer is set to successively     **
**  setup all the demos and make a screenshot of the window area and save it in the ./screenshots          **
**  directory.                                                                                             **
**                                                                                                         **
*************************************************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "basicsettingsdialog.h"
#include "statusdialog.h"
#include "qcustomplot.h" // the header file of QCustomPlot. Don't forget to add it to your project, if you use an IDE, so it gets compiled.

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "definitions.h"
#include "model.h"

#include "statevariables.h"

class Model;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Model *M = 0, int argc=1, QWidget *parent = 0);
    ~MainWindow();
    void setupColorMap(QCustomPlot *customPlot);
    void SetupPlot(int arg);
    void Replot();
    void SetTimestepLabel(int ts);
    void SetTotaltimeLabel(REAL totaltime);
    void save_png();
    void setText_pushButton_start(QString string);
    void SetFluidMassLabel(REAL fluid_mass);
    void SetIntegratedFluxLabel(REAL integrated_flux);
    void SettingsChanged();
    void enable_pushButton_start(bool enable);
    void ChangeApertureRange();

    void ResetRunButton();
    void WindowTitle(QString title);

    void SetSaveInterval();

    bool pause;
    bool autorescale;

private slots:
    void on_pushButton_start_clicked();
    void on_comboBox_currentTextChanged(const QString &arg1);
    void on_pushButton_reset_colormap_clicked();
    void on_pushButton_save_png_clicked();
    void on_pushButton_basic_settings_clicked();
    void on_pushButton_state_clicked();
    void on_pushButton_save_model_clicked();
    void on_pushButton_open_clicked();
    void on_spinBox_save_interval_valueChanged(int arg1);

    void on_checkBox_save_png_stateChanged(int arg1);
    void on_pushButton_make_movie_clicked();
    void on_checkBox_autoscale_stateChanged(int arg1);

    void on_pushButton_open_next_clicked();

    void on_pushButton_open_previous_clicked();

    void on_checkBox_count_fractures_stateChanged();

    void on_checkBox_continuous_signals_stateChanged();

private:
    QString display;
    QString demoName;
    QCPItemTracer *itemDemoPhaseTracer;
    int currentDemoIndex;
    Model *M;
    QCPColorMap *colorMap;
    Ui::MainWindow *ui;

    bool removeDir(const QString & dirName);
};

#endif // MAINWINDOW_H



