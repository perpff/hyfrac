/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[19];
    char stringdata[563];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 27),
QT_MOC_LITERAL(2, 39, 0),
QT_MOC_LITERAL(3, 40, 30),
QT_MOC_LITERAL(4, 71, 4),
QT_MOC_LITERAL(5, 76, 36),
QT_MOC_LITERAL(6, 113, 30),
QT_MOC_LITERAL(7, 144, 36),
QT_MOC_LITERAL(8, 181, 27),
QT_MOC_LITERAL(9, 209, 32),
QT_MOC_LITERAL(10, 242, 26),
QT_MOC_LITERAL(11, 269, 37),
QT_MOC_LITERAL(12, 307, 33),
QT_MOC_LITERAL(13, 341, 32),
QT_MOC_LITERAL(14, 374, 34),
QT_MOC_LITERAL(15, 409, 31),
QT_MOC_LITERAL(16, 441, 35),
QT_MOC_LITERAL(17, 477, 40),
QT_MOC_LITERAL(18, 518, 43)
    },
    "MainWindow\0on_pushButton_start_clicked\0"
    "\0on_comboBox_currentTextChanged\0arg1\0"
    "on_pushButton_reset_colormap_clicked\0"
    "on_pushButton_save_png_clicked\0"
    "on_pushButton_basic_settings_clicked\0"
    "on_pushButton_state_clicked\0"
    "on_pushButton_save_model_clicked\0"
    "on_pushButton_open_clicked\0"
    "on_spinBox_save_interval_valueChanged\0"
    "on_checkBox_save_png_stateChanged\0"
    "on_pushButton_make_movie_clicked\0"
    "on_checkBox_autoscale_stateChanged\0"
    "on_pushButton_open_next_clicked\0"
    "on_pushButton_open_previous_clicked\0"
    "on_checkBox_count_fractures_stateChanged\0"
    "on_checkBox_continuous_signals_stateChanged\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x08,
       3,    1,   95,    2, 0x08,
       5,    0,   98,    2, 0x08,
       6,    0,   99,    2, 0x08,
       7,    0,  100,    2, 0x08,
       8,    0,  101,    2, 0x08,
       9,    0,  102,    2, 0x08,
      10,    0,  103,    2, 0x08,
      11,    1,  104,    2, 0x08,
      12,    1,  107,    2, 0x08,
      13,    0,  110,    2, 0x08,
      14,    1,  111,    2, 0x08,
      15,    0,  114,    2, 0x08,
      16,    0,  115,    2, 0x08,
      17,    0,  116,    2, 0x08,
      18,    0,  117,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->on_pushButton_start_clicked(); break;
        case 1: _t->on_comboBox_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_pushButton_reset_colormap_clicked(); break;
        case 3: _t->on_pushButton_save_png_clicked(); break;
        case 4: _t->on_pushButton_basic_settings_clicked(); break;
        case 5: _t->on_pushButton_state_clicked(); break;
        case 6: _t->on_pushButton_save_model_clicked(); break;
        case 7: _t->on_pushButton_open_clicked(); break;
        case 8: _t->on_spinBox_save_interval_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_checkBox_save_png_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_pushButton_make_movie_clicked(); break;
        case 11: _t->on_checkBox_autoscale_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_pushButton_open_next_clicked(); break;
        case 13: _t->on_pushButton_open_previous_clicked(); break;
        case 14: _t->on_checkBox_count_fractures_stateChanged(); break;
        case 15: _t->on_checkBox_continuous_signals_stateChanged(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
