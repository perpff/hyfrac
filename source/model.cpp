#include "model.h"
#include <iostream>

Model::Model(int argc)
{
    /**************************************************************
    * in case we want to use the program as a viewer, argc is > 1
    **************************************************************/
    dimx = basic_settings.dimx;
    dimy = basic_settings.dimy;
    total_timesteps = basic_settings.total_timesteps;
    total_time = basic_settings.total_time;

    time_passed = 0.0;

    save_number = 1;

    timestep = 0;
    last_timestep_of_previous_loop = 0;

    length_in_x = basic_settings.length_in_x;
    spacing = length_in_x / REAL(dimx);
    dx = spacing;
    dy = spacing;
    length_in_y = - spacing * REAL(dimy);

    dt = dt_initial = total_time/REAL(total_timesteps);

    rate_of_subsidence = basic_settings.rate_of_subsidence;				// m/year

    top_layer_depth = basic_settings.top_layer_depth;
    bottom_layer_depth = top_layer_depth - length_in_y;

	fluid_mass_lost = 0.0;

    // directory stucrture
	QDir statistics_dir("statistics");
	if (!statistics_dir.exists()) {
		statistics_dir.mkpath(".");
	}
	else
	{
		cout << "Do you _really_ want to overwrite a previous experiment???" << endl << endl;
		cout << "If you wish to proceed with the simulation, delete the statistics directory first!" << endl;
		
		exit(0);
	}
	
	QDir output_dir("output");
	if (!output_dir.exists()) {
		output_dir.mkpath(".");
	}

    Nodes = new Node *[dimx];
    for (int i=0; i<dimx; i++)
        Nodes[i] = new Node [dimy];

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            Nodes[i][j].InitNode(dx, dy, top_layer_depth, dt, i, j, dimx, dimy);
        }
    }

    /* **************************************************
        * preliminary settings for the explicit solution,
        * so we don't have to do this every time the solution
        * function is called
        * *************************************************/
    // 1D Array to store the nodes
    int number_of_nodes = dimx * dimy;

    NodeNeighbours = new Node**[number_of_nodes];
    for(int i=0; i<number_of_nodes; i++)
        NodeNeighbours[i] = new Node*[4]; // Each unode has 4 neighbouring positions

    NodeList = new Node*[number_of_nodes];

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            NodeList [j*dimx + i] = &Nodes[i][j];
        }
    }

    // a vector with pointers to the nodes, used to randomize the order of nodes in the explicit solution
    ran.clear();
    for ( int j = 0; j < number_of_nodes; j++ )
            ran.push_back( NodeList[j] );

    // this variable could be set in the InitSimulation function
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            if (j>0)
                NodeNeighbours[j*dimx + i][0] = &Nodes[i][j-1];
            else
                NodeNeighbours[j*dimx + i][0] = NULL;

            if (i<dimx-1)
                NodeNeighbours[j*dimx + i][1] = &Nodes[i+1][j];
            else
                NodeNeighbours[j*dimx + i][1] = &Nodes[0][j];
//                NodeNeighbours[j*dimx + i][1] = NULL;

            if (j < dimy-1)
                NodeNeighbours[j*dimx + i][2] = &Nodes[i][j+1];
            else
                NodeNeighbours[j*dimx + i][2] = NULL;

            if (i > 0)
                NodeNeighbours[j*dimx + i][3] = &Nodes[i-1][j];
            else
                NodeNeighbours[j*dimx + i][3] = &Nodes[dimx-1][j];
//                NodeNeighbours[j*dimx + i][3] = NULL;
        }
    }

    fractures = new Fractures(this);

    if (argc == 1)
    {
        InitSimulation();
    }

    saved_timestep = 1;
    running = false;
    stop_adding_water = false;

    model_file = "";

    if (bottom_layer_depth < 3.0*spacing)
    {
        cout << "The model is not deep enought for this resolution" << endl;
        exit (0);
    }
	
	fluid_mass = CalcSystemFluidMass();
}

void Model::RunMainLoop(MainWindow *W)
{

    state_variables.fluid_mass = CalcSystemFluidMass();

    for (timestep=saved_timestep + last_timestep_of_previous_loop; timestep <= total_timesteps + last_timestep_of_previous_loop; timestep++)

    {
        if (!W->pause)
        {
            // update the interface
            W->SetTimestepLabel(timestep);
            W->SetTotaltimeLabel(REAL((time_passed) / (365*86400)));

            W->Replot();

            // the actual simulation. The program flow should be controlled from there.
            Simulation(W);

            W->Replot();

            if (!basic_settings.count_fractures_to_save_file)
            {
                if (basic_settings.SaveInterval > 0)
                {
                    if (timestep % basic_settings.SaveInterval == 0)
                    {
                        SaveModel();

                        if (basic_settings.save_png)
                            W->save_png();

                        save_number++;
                    }
                }
            }
            else
            {
                SaveModel();

                if (basic_settings.save_png)
                    W->save_png();

                save_number++;
            }

            state_variables.fluid_mass_loss = state_variables.fluid_mass;
            state_variables.fluid_mass = CalcSystemFluidMass();
//            cout << "fluid mass: " << state_variables.fluid_mass << endl;
            state_variables.fluid_mass_loss = state_variables.fluid_mass_loss - state_variables.fluid_mass;

			SaveStatistics();
			DumpFlowrate();	// flowrate is for the entire timestep and will not be called together with the other statistics, which are calculated for every single dynamic timestep
			

        }
        else
        {
            saved_timestep = timestep - last_timestep_of_previous_loop - 1;
            break;
        }
    }

    if (!W->pause) // this means, if the time is up and not pause is pressed
    {
        W->ResetRunButton();
        last_timestep_of_previous_loop += total_timesteps;
        saved_timestep = 0;
    }
}

void Model::SaveModel()
{
    QString file = QDir::currentPath() + "/output/" + "timestep_" + QString::number(save_number) + ".mod";

    QFile savefile(file);
    savefile.open(QIODevice::WriteOnly);
    QDataStream out(&savefile);
    out.setVersion(QDataStream::Qt_5_1);

    out << dx << dy << dt << time_passed << dimx << dimy << timestep << top_layer_depth << bottom_layer_depth << basic_settings.count_fractures_to_save_file << basic_settings.SaveInterval;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            out << GetConductivityInX(i, j);
            out << GetConductivityInY(i, j);
            //~ out << GetCorrectedFluidPressure(i, j);
            //~ out << GetDiffusivityInX(i, j);
            //~ out << GetDiffusivityInY(i, j);
            out << GetEffectivePressure(i, j);
            out << GetFluidPressure(i, j);
            out << GetFractureStatus(i, j);
            out << GetHydraulicHead(i, j);
            out << GetTotalPressure(i, j);
            out << GetPorosity(i, j);
            out << GetSx(i, j);
            out << GetSy(i, j);
            out << GetTemperature(i, j);
            out << GetViscosity(i, j);
            //~ out << GetFractureNumber(i, j);
            //~ out << GetSolidPressure(i, j);
            out << GetXApertureRatio(i, j);
            out << GetYApertureRatio(i, j);
            out << GetBrokenInX(i, j);
            out << GetBrokenInY(i, j);
            //~ out << GetFluidMass(i, j);
            //~ out << GetXDamage(i, j);
            //~ out << GetYDamage(i, j);
            //~ out << GetTensileStrengthX(i, j);
            //~ out << GetTensileStrengthY(i, j);
        }
    }
}

void Model::OpenModel(MainWindow *W, QString filename)
{
    QString time;

    if (filename.isEmpty())
        filename = QFileDialog::getOpenFileName(W, QString("Open model-file"), ".", QString("Model-files (*.mod)"));

    QFile open_file (filename);

    if (open_file.exists())
    {
        model_file = filename;
        W->enable_pushButton_start(false);
        W->WindowTitle("View model files");

        time = filename;
        time.remove(time.lastIndexOf("."), 4);
        time.remove(0, time.lastIndexOf("_")+1);

        timestep = time.toInt();

        open_file.open(QIODevice::ReadOnly);
        QDataStream in(&open_file);
        in.setVersion(QDataStream::Qt_5_1);

        if (Nodes[0])
        {
            // delete the old Nodes-array
            for (int i=0; i<dimx; i++)
                delete Nodes[i];
            delete Nodes;
        }

        in >> dx >> dy >> dt >> time_passed >> dimx >> dimy >> timestep >> top_layer_depth >> bottom_layer_depth >> basic_settings.count_fractures_to_save_file >> basic_settings.SaveInterval;

        // create the new Nodes array
        Nodes = new Node *[dimx];
        for (int i=0; i<dimx; i++)
            Nodes[i] = new Node [dimy];

        for (int i=0; i<dimx; i++)
        {
            for (int j=0; j<dimy; j++)
            {
                Nodes[i][j].InitNode(dx, dy, top_layer_depth, dt, i, j, dimx, dimy);
            }
        }

        for (int i=0; i<dimx; i++)
        {
            for (int j=0; j<dimy; j++)
            {
                in >> Nodes[i][j].x_conductivity;
                in >> Nodes[i][j].y_conductivity;
                //~ in >> Nodes[i][j].P_corrected;
                //~ in >> Nodes[i][j].x_diffusivity;
                //~ in >> Nodes[i][j].y_diffusivity;
                in >> Nodes[i][j].P_effective;
                in >> Nodes[i][j].P;
                in >> Nodes[i][j].fracture;
                in >> Nodes[i][j].head;
                in >> Nodes[i][j].P_total;
                in >> Nodes[i][j].porosity;
                in >> Nodes[i][j].sx;
                in >> Nodes[i][j].sy;
                in >> Nodes[i][j].temperature;
                in >> Nodes[i][j].rock_viscosity;
                //~ in >> Nodes[i][j].fracture_number;
                //~ in >> Nodes[i][j].P_solid;
                in >> Nodes[i][j].x_aperture;
                in >> Nodes[i][j].y_aperture;
                in >> Nodes[i][j].broken_in_x;
                in >> Nodes[i][j].broken_in_y;
                //~ in >> Nodes[i][j].fluid_mass;
                //~ in >> Nodes[i][j].x_damage;
                //~ in >> Nodes[i][j].y_damage;
                //~ in >> Nodes[i][j].tensile_strength_x;
                //~ in >> Nodes[i][j].tensile_strength_y;
            }
        }

        W->Replot();
        W->SetTimestepLabel(timestep);
        W->SetTotaltimeLabel(REAL((time_passed) / (365*86400)));
        W->SetSaveInterval();
    }
}

REAL Model::GetOverPressure(int i, int j)
{
    return ((Nodes[i][j].sy + Nodes[i][j].sx)/2.0 - Nodes[i][j].P);
}

REAL Model::GetXApertureRatio(int i, int j)
{
    REAL ratio = Nodes[i][j].x_aperture / spacing;
    return(ratio);
}

REAL Model::GetYApertureRatio(int i, int j)
{
    REAL ratio = Nodes[i][j].y_aperture / spacing;
    return(ratio);
}

void Model::ChangeSaveInterval(int interval)
{
    basic_settings.SaveInterval = interval;
}

REAL Model::GetHydraulicHead(int i, int j)
{
    REAL head = Nodes[i][j].head;
    if (head < 1e-10) head = 0.0;

    return(head);
}

REAL Model::GetFluidPressure(int i, int j)
{
    return(Nodes[i][j].P);
}

REAL Model::GetSolidPressure(int i, int j)
{
    return(Nodes[i][j].P_solid);
}

REAL Model::GetTensileStrengthX(int i, int j)
{
	return(Nodes[i][j].tensile_strength_x);
}

REAL Model::GetTensileStrengthY(int i, int j)
{
	return(Nodes[i][j].tensile_strength_y);
}

REAL Model::GetCorrectedFluidPressure(int i, int j)
{
    REAL P_cor = Nodes[i][j].P_corrected;
    if (P_cor < 1e-10 && P_cor > -1e-10) P_cor = 0.0;

    return(P_cor);
}

REAL Model::GetDiffusivityInX(int i, int j)
{
    return(Nodes[i][j].x_diffusivity);
}

REAL Model::GetDiffusivityInY(int i, int j)
{
    return(Nodes[i][j].y_diffusivity);
}

REAL Model::GetEffectivePressure(int i, int j)
{
    return(Nodes[i][j].P_effective);
}

REAL Model::GetSx(int i, int j)
{
    return(Nodes[i][j].sx);
}

REAL Model::GetSy(int i, int j)
{
    return(Nodes[i][j].sy);
}

int Model::GetTimestep()
{
    return (timestep);
}

REAL Model::GetConductivityInX(int i, int j)
{
    return (Nodes[i][j].x_conductivity);
}

REAL Model::GetConductivityInY(int i, int j)
{
    return (Nodes[i][j].y_conductivity);
}

REAL Model::GetViscosity(int i, int j)
{
    return (Nodes[i][j].rock_viscosity);
}

REAL Model::GetPorosity(int i, int j)
{
    return (Nodes[i][j].porosity);
}

REAL Model::GetTemperature(int i, int j)
{
    return (Nodes[i][j].temperature);
}

REAL Model::GetTotalPressure(int i, int j)
{
    return (Nodes[i][j].P_total);
}

int Model::GetFractureStatus(int i, int j)
{
    return (Nodes[i][j].fracture);
}

REAL Model::GetFluidMass(int i, int j)
{
    return (Nodes[i][j].fluid_mass);
}

bool Model::GetBrokenInX(int i, int j)
{
   return (Nodes[i][j].broken_in_x);
}

bool Model::GetBrokenInY(int i, int j)
{
   return (Nodes[i][j].broken_in_y);
}

REAL Model::GetXDamage(int i, int j)
{
   return (Nodes[i][j].x_damage);
}

REAL Model::GetYDamage(int i, int j)
{
   return (Nodes[i][j].y_damage);
}

Node* Model::GetNodeNeighbour(int nodeposition, int neighbourposition)
{
    return (NodeNeighbours[nodeposition][neighbourposition]);
}

int Model::GetFractureNumber(int i, int j)
{
    return (Nodes[i][j].fracture_number);
}

int Model::GetFractureActivity(bool active, int i, int j)
{
    Node *nd;
    bool val = false;

    nd = &(Nodes[i][j]);

    if (nd->fracture)
    {
        if (!active)
        {
            if (nd->broken_in_x && nd->P < nd->sx)
                val = true;

            if (nd->broken_in_y && nd->P < nd->sy)
                val = true;
        }
        else
        {
            if (nd->broken_in_x && nd->P > nd->sx)
                val = true;

            if (nd->broken_in_y && nd->P > nd->sy)
                val = true;
        }
    }

    if (val)
        return (1);
    else
        return (0);
}

void Model::SaveStatistics()
{
	SaveFractureRatio();
	SaveOutflow();
	DumpFluidPressure();
	DumpCorrectedFluidPressure();
	DumpEffectivePressure();
}

void Model::DumpFluidPressure()
{
	// save the fluid pressure for the entire system + for each row
    QString file = QDir::currentPath() + "/statistics/" + "fluid_pressure.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);

	REAL fluid_pressure = 0.0;

	out << timestep << "," << time_passed << "," << dt << ",";

    for (int i=0; i<dimy; i++)
    {
		
        for (int j=0; j<dimx; j++)
        {
			fluid_pressure += Nodes[j][i].P;
        }
    }
    
    out << fluid_pressure / (dimx*dimy);
    
    for (int i=0; i<dimy; i++)
    {
		
		fluid_pressure = 0.0;
		
        for (int j=0; j<dimx; j++)
            fluid_pressure += Nodes[j][i].P;
        
        out << "," << fluid_pressure / dimx;
    }
    
    out << endl;
	
}

void Model::DumpCorrectedFluidPressure()
{
	// save the fluid pressure for the entire system + for each row
    QString file = QDir::currentPath() + "/statistics/" + "corrected_fluid_pressure.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);

	REAL fluid_pressure = 0.0;

	out << timestep << "," << time_passed << "," << dt << ",";

    for (int i=0; i<dimy; i++)
    {
		
        for (int j=0; j<dimx; j++)
        {
			fluid_pressure += Nodes[j][i].P;
        }
    }
    
    out << fluid_pressure / (dimx*dimy);
    
    for (int i=0; i<dimy; i++)
    {
		
		fluid_pressure = 0.0;
		
        for (int j=0; j<dimx; j++)
            fluid_pressure += Nodes[j][i].P_corrected;
        
        out << "," << fluid_pressure / dimx;
    }
    
    out << endl;
}

void Model::DumpEffectivePressure()
{
	// save the fluid pressure for the entire system + for each row
    QString file = QDir::currentPath() + "/statistics/" + "effective_pressure.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);

	REAL fluid_pressure = 0.0;

	out << timestep << "," << time_passed << "," << dt << ",";

    for (int i=0; i<dimy; i++)
    {
		
        for (int j=0; j<dimx; j++)
        {
			fluid_pressure += Nodes[j][i].P_effective;
        }
    }
    
    out << fluid_pressure / (dimx*dimy);
    
    for (int i=0; i<dimy; i++)
    {
		
		fluid_pressure = 0.0;
		
        for (int j=0; j<dimx; j++)
            fluid_pressure += Nodes[j][i].P_effective;
        
        out << "," << fluid_pressure / dimx;
    }
    
    out << endl;	
}

// call this function __only__ from SaveStatistics!!!
void Model::SaveFractureRatio()
{
    QString file = QDir::currentPath() + "/statistics/" + "fracture_ratio.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);

    int count = 0;
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            if (Nodes[i][j].fracture)
                count++;
        }
    }

    out << timestep << "," << time_passed << "," << dt << "," << REAL(count)/REAL(dimx*dimy) << "," << count << "," << dimx*dimy << endl;

}

// call this function __only__ from SaveStatistics!!!
void Model::SaveOutflow()
{
    QString file = QDir::currentPath() + "/statistics/" + "flow.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);
    
    fluid_mass_previous = fluid_mass;
    fluid_mass = CalcSystemFluidMass();

    out << timestep << "," << time_passed << "," << dt << "," << fluid_mass << "," << (fluid_mass_previous - fluid_mass) / length_in_x << endl;

}

void Model::InitFluidPressureFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "fluid_pressure.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Time,Delta t,Average";
    
    for (int i=0; i<dimy; i++)
    {
		out << "," << i;
	}
	
	out << endl;
}

void Model::InitEffectivePressureFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "effective_pressure.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Time,Delta t,Average";

    for (int i=0; i<dimy; i++)
    {
		out << "," << i;
	}
	
	out << endl;
}

void Model::InitCorrectedFluidPressureFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "corrected_fluid_pressure.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Time,Delta t,Average";

    for (int i=0; i<dimy; i++)
    {
		out << "," << i;
	}
	
	out << endl;
}

void Model::InitFractureRatioFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "fracture_ratio.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Time,Delta t,Ratio,Fractured nodes,Total nodes" << endl;
}

void Model::InitOutflowFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "flow.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Total time,Delta t,Fluid mass, Fluid mass difference between production and loss in m^3/m^2" << endl;
}

void Model::InitFlowrateFile()
{
    QString file = QDir::currentPath() + "/statistics/" + "flowrate.csv";

    if (QFileInfo::exists(file))
        QFile::remove(file);

    QFile openfile(file);
    openfile.open(QIODevice::WriteOnly);
    QTextStream out(&openfile);

    out << "Timestep,Total time,last Delta t, fluid mass loss in m^3/(m^2*Delta t)" << endl;
}

void Model::DumpFlowrate()
{
    QString file = QDir::currentPath() + "/statistics/" + "flowrate.csv";

    QFile savefile(file);
    savefile.open(QIODevice::Append);
    QTextStream out(&savefile);

    out << timestep << "," << time_passed << "," << dt_initial << "," << fluid_mass_lost / (length_in_x * dt_initial) << endl;
    
    fluid_mass_lost = 0.0; // fluid mass will be summed up in the solution function

}

void Zip (QString filename , QString zipfilename){

    QFile infile(filename);
    QFile outfile(zipfilename);
    infile.open(QIODevice::ReadOnly);
    outfile.open(QIODevice::WriteOnly);
    QByteArray uncompressedData = infile.readAll();
    QByteArray compressedData = qCompress(uncompressedData,9);
    outfile.write(compressedData);
    infile.close();
    outfile.close();
}

void Unzip (QString zipfilename , QString filename){
    QFile infile(zipfilename);
    QFile outfile(filename);
    infile.open(QIODevice::ReadOnly);
    outfile.open(QIODevice::WriteOnly);
    QByteArray uncompressedData = infile.readAll();
    QByteArray compressedData = qUncompress(uncompressedData);
    outfile.write(compressedData);
    infile.close();
    outfile.close();
}
