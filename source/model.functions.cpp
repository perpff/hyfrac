#include "model.h"

using namespace std;

void Model::Lift()
{
    if (rate_of_subsidence != 0.0)
    {
        UpdateModelDimensions();
        UpdateSolidStress();
        UpdateTemperature();
        UpdateViscosity();
    }
}

void Model::AdjustChangeOfHorizontalStressToFluidPressure()
{
	
    // Hillis, 2003
    REAL poisson = 0.3333;
    REAL k = poisson / (1.0 - poisson); // if poisson = 1/3, then k = 1/2

    REAL d_P;
    REAL d_s = 0.0;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            d_P = Nodes[i][j].P - Nodes[i][j].prev_P;
            d_s = (1.0 - k) * d_P;

            Nodes[i][j].sx += d_s;

            if (Nodes[i][j].sx < 0.0)
                Nodes[i][j].sx = 0.0;

            Nodes[i][j].P_solid = (Nodes[i][j].sy + Nodes[i][j].sx) / 2.0;
            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity) * Nodes[i][j].P_solid + Nodes[i][j].porosity * Nodes[i][j].P;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            Nodes[i][j].P_corrected = Nodes[i][j].P - water_density * g * Nodes[i][j].depth;

            Nodes[i][j].prev_P = Nodes[i][j].P;
        }
    }
}

void Model::CalculateHorizontalStressFromFluidPressure()
{
    // Hillis, 2003
    REAL poisson = 0.3333;
    REAL k = poisson / (1.0 - poisson); // if poisson = 1/3, then k = 1/2

    REAL d_P;
    REAL d_s = 0.0;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            d_P = Nodes[i][j].P - Nodes[i][j].prev_P;
            d_s = (1.0 - k) * d_P;

            Nodes[i][j].sx += d_s;

            if (Nodes[i][j].sx < 0.0)
                Nodes[i][j].sx = 0.0;

            Nodes[i][j].P_solid = (Nodes[i][j].sy + Nodes[i][j].sx) / 2.0;
            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity) * Nodes[i][j].P_solid + Nodes[i][j].porosity * Nodes[i][j].P;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            Nodes[i][j].P_corrected = Nodes[i][j].P - water_density * g * Nodes[i][j].depth;

            Nodes[i][j].prev_P = Nodes[i][j].P;
        }
    }
}



void Model::AddNormalDistributedRandomFluidBatches(REAL total_amount_per_second, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;
    REAL g_mean = 1.0;
    
    REAL **factor;

    factor = new REAL *[dimx];
    for (int i=0; i<dimx; i++)
        factor[i] = new REAL [dimy];
    
    for (int i=0; i<dimx; i++)
    {
    	for (int j=0; j<dimy; j++)
    	{
    		factor[i][j] = 0.0;
    	}
    }
    
    REAL total_amount = total_amount_per_second * dt;

    cout << total_amount << endl;

    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        factor[i][j] = p_rand ;
                        break;
                    }
                }
            }
            while (1);
        }
    }

    REAL sum = 0.0;
    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            sum += factor[i][j];
        }
    }

    REAL inc;
    inc = total_amount / sum;

    REAL buf;
    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            buf = inc * factor[i][j];

            node = &(Nodes[i][j]);

            buf = (node->fluid_density*node->porosity*node->volume + buf) / (node->porosity * node->volume);
            node->P_last = node->P;
            node->P = node->P - water_bulk_modulus * (node->fluid_density/buf - 1.0);

            node->P_total = (1.0 - Nodes[i][j].porosity) * Nodes[i][j].P_solid + Nodes[i][j].porosity * Nodes[i][j].P;
            node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            node->P_corrected = Nodes[i][j].P - water_density * g * Nodes[i][j].depth;

            node->fluid_density = buf;
        }
    }
}


void Model::SetConductivityGradient(REAL top_cond, REAL bottom_cond)
{
	Node* nd;
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
			nd = &Nodes[i][j];
			
			nd->y_conductivity = nd->y_conductivity_initial = -(nd->depth / length_in_y) * (bottom_cond - top_cond) + top_cond;
			nd->x_conductivity = nd->x_conductivity_initial = -(nd->depth / length_in_y) * (bottom_cond - top_cond) + top_cond;

		}
	}
}


void Model::AddNormalDistributedFluidAtBottom(REAL total_amount_per_anno, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;
    REAL g_mean = 1.0;
    
    REAL time = dt / a;
    REAL total_amount = total_amount_per_anno * time;
	REAL amount_per_node = total_amount / REAL(dimx);
	
	REAL rand_factor = 1.0;
	
	REAL released_volume, rho0, pore_volume, bulk, fluid_mass_0, fluid_mass_1, added_fluid_mass;
    
	// convert release fluid mass to excess pressure
	bulk = 1.0 / water_compressibility_base;

    
    for (int i = 0; i < dimx; i++)
    {
			            
            node = &(Nodes[i][0]);
            
			do
			{
				p_rand = rand () / REAL(RAND_MAX);

				p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
				p_rand = p_rand + g_mean;

				prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
				prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
				prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

				ran_nb = rand () / REAL(RAND_MAX);

				if (ran_nb <= prob)
				{
					if (p_rand <= 0.0)
						;
					else
					{
						rand_factor = p_rand ;
						break;
					}
				}
			}
			while (1);
	   
			added_fluid_mass = amount_per_node * rand_factor;
										
			rho0 = - (water_density * bulk) / (node->P - bulk); // node->P * water_density / bulk, P_0 = 0 and rho_0 is the common density
									
			pore_volume = node->porosity * node->volume;
			fluid_mass_0 = rho0 * pore_volume;
			
			fluid_mass_1 = fluid_mass_0 + added_fluid_mass;
			
			//cout << "dP: " << bulk * (fluid_mass_0 / fluid_mass_1 - 1.0) << endl;
			
			node->P = node->P - bulk * (fluid_mass_0 / fluid_mass_1 - 1.0);
			
			node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

			node->P_corrected = node->P - node->depth * g * water_density;
			node->P_effective = (node->sx + node->sy)/2.0 - node->P;
			node->head = node->P - node->depth*g*water_density;
	}
}


void Model::SetConstantOverPressureBottomRow(REAL value)
{
	Node *node;
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<5; j++)
		{
			node = &Nodes[i][j];
			
			node->P = (node->sy + node->sx)/2.0 - value;
			
			node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;			
			node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
			
			node->P_corrected = node->P - node->depth * g * water_density;
			node->head = -node->depth*g*water_density + node->P;

            node->preserve_pressure = true;
		}
	}
}

void Model::AddNormalDistributedFluidPressureIncrement(REAL inc, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;
    REAL g_mean = 1.0;
	
	REAL rand_factor = 1.0;
	
	REAL released_volume, rho0, pore_volume, bulk, fluid_mass_0, fluid_mass_1, added_fluid_mass;
    
    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
			            
            node = &(Nodes[i][j]);

			do
			{
				p_rand = rand () / REAL(RAND_MAX);

				p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
				p_rand = p_rand + g_mean;

				prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
				prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
				prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

				ran_nb = rand () / REAL(RAND_MAX);

				if (ran_nb <= prob)
				{
					if (p_rand <= 0.0)
						;
					else
					{
						rand_factor = p_rand ;
						break;
					}
				}
			}
			while (1);
					
			node->P += inc * rand_factor;
			
			node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

			node->P_corrected = node->P - node->depth * g * water_density;
			node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
			node->head = node->P - node->depth*g*water_density;
        }
    }
}

void Model::AddNormalDistributedFluid(REAL fluid_release_mass_per_anno_per_square_meter, REAL g_sigma, REAL top, REAL bottom)
{
    Node *node;
    REAL p_rand, prob, ran_nb;
    REAL g_mean = 1.0;
    
    REAL time = dt / a;
    REAL total_amount = fluid_release_mass_per_anno_per_square_meter * time * length_in_x;
	
	int node_counter;
    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {		            
            node = &(Nodes[i][j]);

			if (node->depth >= top && node->depth <= bottom)
				node_counter++;
		}
	}
					
	REAL amount_per_node = total_amount / REAL(node_counter);
	
	REAL rand_factor = 1.0;
	
	REAL released_volume, rho0, pore_volume, bulk, fluid_mass_0, fluid_mass_1, added_fluid_mass;
    
    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {		            
            node = &(Nodes[i][j]);

			if (node->depth >= top && node->depth <= bottom)
			{
				do
				{
					p_rand = rand () / REAL(RAND_MAX);

					p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
					p_rand = p_rand + g_mean;

					prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
					prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
					prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

					ran_nb = rand () / REAL(RAND_MAX);

					if (ran_nb <= prob)
					{
						if (p_rand <= 0.0)
							;
						else
						{
							rand_factor = p_rand ;
							break;
						}
					}
				}
				while (1);
		   
				added_fluid_mass = amount_per_node * rand_factor;
						
				// convert release fluid mass to excess pressure
				bulk = 1.0 / water_compressibility_base;
				
				rho0 = - (water_density * bulk) / (node->P - bulk); // node->P * water_density / bulk, P_0 = 0 and rho_0 is the common density
										
				pore_volume = node->porosity * node->volume;
				fluid_mass_0 = rho0 * pore_volume;
				
				fluid_mass_1 = fluid_mass_0 + added_fluid_mass;
				
				//cout << "dP: " << bulk * (fluid_mass_0 / fluid_mass_1 - 1.0) << endl;
				
				node->P = node->P - bulk * (fluid_mass_0 / fluid_mass_1 - 1.0);
				
				node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

				node->P_corrected = node->P - node->depth * g * water_density;
				node->P_effective = (node->sx + node->sy)/2.0 - node->P;
				node->head = node->P - node->depth*g*water_density;
			}
        }
    }
}

// calcs the aperture and from the aperture the 'damage' in x and y and from the 'damage' the permeability / conductivity
void Model::CalcFractureAperture(bool close_fractures, bool use_crack_formula)
{

    REAL sigma_n_max = 0.0;
    Node *nd;
    
    // these are needed in order to call the fracture-conductivity function
    REAL x_frac_perm; 
    REAL x_frac_cond; 
    REAL y_frac_perm; 
    REAL y_frac_cond;

    REAL new_aperture;
    
    REAL distance_affected_by_fracturing = 0.0;
    
    REAL k; // spring constant

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            nd = &(Nodes[i][j]);

			if (!nd->top_boundary)
			{
				if (use_crack_formula)
				{

					if (nd->broken_in_x)
					{
						sigma_n_max = nd->P - nd->sx;
						new_aperture = sigma_n_max * 4.0 * (1.0 - crustal_poisson_ratio*crustal_poisson_ratio) / E_Young * spacing;

						// new approach with spring constant                    
						//k = E_Young * spacing / spacing; // reducing the fraction is trivial, just a reminder
						//new_aperture = sigma_n_max * spacing * k;
						
						if (new_aperture > spacing)
							new_aperture = spacing;

						if (close_fractures)
						{
							nd->x_aperture = new_aperture;
						}
						else
						{
							if (new_aperture > nd->x_aperture)
							{
								nd->x_aperture = new_aperture;
							}
						}

						if (nd->x_aperture <= 0.0)
						{      						
							nd->x_aperture = 0.0;
							nd->broken_in_x = false;
						}
					}

					if (nd->broken_in_y)
					{
						sigma_n_max = nd->P - nd->sy;
						new_aperture = sigma_n_max * 4.0 * (1.0 - crustal_poisson_ratio*crustal_poisson_ratio) / E_Young * spacing;

						// new approach with spring constant                    
						//k = E_Young * spacing / spacing; // reducing the fraction is trivial, just a reminder
						//new_aperture = sigma_n_max * spacing * k;
						
						if (new_aperture > spacing)
							new_aperture = spacing;

						if (close_fractures)
						{
							nd->y_aperture = new_aperture;
						}
						else
						{
							if (new_aperture > nd->y_aperture)
							{
								nd->y_aperture = new_aperture;
							}
						}

						if (nd->y_aperture <= 0.0)
						{      						
							nd->y_aperture = 0.0;
							nd->broken_in_y = false;
						}
					}
				}
				else
				{
					if (nd->broken_in_x)
					{
						sigma_n_max = nd->P - nd->sx;
						new_aperture = sigma_n_max * spacing / E_Young;

						if (close_fractures)
						{
							nd->x_aperture = new_aperture;
						}
						else
						{
							if (new_aperture > nd->x_aperture)
							{
								nd->x_aperture = new_aperture;
							}
						}

						if (nd->x_aperture <= 0.0)
						{
							
							nd->x_aperture = 0.0;
							nd->broken_in_x = false;
						}
					}

					if (nd->broken_in_y)
					{
						sigma_n_max = nd->P - nd->sy;
						new_aperture = sigma_n_max * spacing / E_Young;

						if (close_fractures)
						{
							nd->y_aperture = new_aperture;
						}
						else
						{
							if (new_aperture > nd->y_aperture)
							{
								nd->y_aperture = new_aperture;
							}
						}

						if (nd->y_aperture <= 0.0)
						{                        

							nd->y_aperture = 0.0;

								nd->broken_in_y = false;
						}
					}
				}

				if (!nd->broken_in_x && !nd->broken_in_y)
				{
					nd->fracture = false;
				}
				
				// calculate the 'damage'
				if (nd->broken_in_x)
				{
					distance_affected_by_fracturing = nd->x_aperture / basic_settings.fracture_porosity;
					
					nd->x_damage = distance_affected_by_fracturing / dx;
					
					if (nd->x_damage < 0.0)
						nd->x_damage = 0.0;
					if (nd->x_damage > 1.0)
						nd->x_damage = 1.0;
				}
				else
					nd->x_damage = 0.0;
				
				if (nd->broken_in_y)
				{
					distance_affected_by_fracturing = nd->y_aperture / basic_settings.fracture_porosity;
					
					nd->y_damage = distance_affected_by_fracturing / dy;
					
					if (nd->y_damage < 0.0)
						nd->y_damage = 0.0;
					if (nd->y_damage > 1.0)
						nd->y_damage = 1.0;
				}
				else
					nd->y_damage = 0.0;
			}
		}
    }
}

void Model::SealFractures()
{
    Node *nd;
    REAL sx, sy;
    REAL dl;
    REAL l0;

    REAL viscosity;

    REAL A = basic_settings.rock_viscosity_A;
    REAL E = basic_settings.rock_viscosity_E;
    REAL exponent = basic_settings.rock_viscosity_n;
    REAL V = basic_settings.rock_viscosity_V;
    
	REAL x_frac_perm; 
	REAL x_frac_cond; 
	REAL y_frac_perm; 
	REAL y_frac_cond;

    REAL T;

    REAL diff_stress;
    
    REAL k, tau;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            nd = &(Nodes[i][j]);

            if (!nd->top_boundary)
            {

                if (nd->fracture)
                {

                    if (nd->broken_in_x)
                    {
                        sx = (nd->sx - nd->P);

                        if (basic_settings.fixed_fracture_viscosity)
                            viscosity = basic_settings.fracture_viscosity;
                        else
                        {
                            viscosity = GetViscosity(nd->temperature, sx, A, exponent, E, V);
                        }

                        if (sx > 0)
                        {
                            l0 = spacing - nd->x_aperture;

                            if (basic_settings.viscoelastic)
                            {
                                k = sx / nd->young;
                                tau = viscosity / nd->young;
							
                                tau = tau / basic_settings.shape_factor_fracture_closure;
							
                                //dl = l0 * dt * sx / viscosity;
                                dl = spacing - l0*(exp(k*(exp(-dt/tau)-1)));
                                //cout << "dl: " << dl << endl;
                                nd->x_aperture -= dl;
                            }
                            else // viscous
                            {
                                nd->x_aperture = spacing - l0*(exp(sx/viscosity * dt));
                            }

                            if (nd->x_aperture <= 0.0)
                            {
                                nd->x_aperture = 0.0;
                                nd->broken_in_x = false;
                            }
                        }
                    }

                    if (nd->broken_in_y)
                    {
                        sy = (nd->sy - nd->P);

                        if (basic_settings.fixed_fracture_viscosity)
                            viscosity = basic_settings.fracture_viscosity;
                        else
                        {
                            viscosity = GetViscosity(nd->temperature, sy, A, exponent, E, V);
                        }

                        if (sy > 0)
                        {
                            l0 = spacing - nd->y_aperture;

                            if (basic_settings.viscoelastic)
                            {
                                k = sy / nd->young;
                                tau = viscosity / nd->young;
							
                                tau = tau / basic_settings.shape_factor_fracture_closure;
							
                                //dl = l0 * dt * sx / viscosity;
                                dl = spacing - l0*(exp(k*(exp(-dt/tau)-1)));
                                //cout << "dl: " << dl << endl;
                                nd->y_aperture -= dl;
                            }
                            else // viscous
                            {
                                nd->y_aperture = spacing - l0*(exp(sy/viscosity * dt));
                            }

                            if (nd->y_aperture <= 0.0)
                            {
                                nd->y_aperture = 0.0;
                                nd->broken_in_y = false;
                            }
                        }
                    }

                    if (!nd->broken_in_x && !nd->broken_in_y)
                    {
                        nd->fracture = false;
                    }
                }
            }
        }
    }
}

void Model::UpdateModelDimensions()
{
    REAL lift = dt * rate_of_subsidence;

    if (rate_of_subsidence < 0) // this means UPLIFT
    {
        top_layer_depth += lift;
        bottom_layer_depth += lift;

        for (int i=0; i<dimx; i++)
        {
            for (int j=0; j<dimy; j++)
            {
                Nodes[i][j].real_ypos -= lift;
                Nodes[i][j].depth = - Nodes[i][j].real_ypos;
            }
        }


        // workaround if the profile is uplifted "above the surface",
        // in this case we have to remove the corresponding rows of particles
        if (top_layer_depth <= 0.0)
        {
            top_layer_depth = 0.0;

            if (Nodes[0][dimy-1].depth < 0.0)
            {
                dimy -= 1;

                // because the sign of the y-length matters, we subtract the top_layer from the bottom_layer
                length_in_y = bottom_layer_depth - top_layer_depth;


                if (bottom_layer_depth < 3.0 * spacing)
                {
                    cout << "the simulation exited, because the bottom of the profile is now above the surface" << endl;
                    exit (0);
                }
            }
        }

    }
}

void Model::UpdateSolidStress()
{
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            Nodes[i][j].sy = Nodes[i][j].depth * av_crustal_density * g;
            Nodes[i][j].sx = Nodes[i][j].sy; //0.33 / (1.0 - 0.33) * Nodes[i][j].sy;
            Nodes[i][j].sxy = 0;

            if (Nodes[i][j].top_boundary)
            {
                Nodes[i][j].P_last = Nodes[i][j].P;
                Nodes[i][j].P = water_density * g * Nodes[i][j].depth;
            }

            Nodes[i][j].P_solid = (Nodes[i][j].sy + Nodes[i][j].sx) / 2.0;
            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity) * Nodes[i][j].P_solid + Nodes[i][j].porosity * Nodes[i][j].P;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            Nodes[i][j].P_corrected = Nodes[i][j].P - water_density * g * Nodes[i][j].depth;
        }
    }
}

void Model::UpdateTemperature()
{

}

void Model::SetTopBoundariesAsPermanentFaults(int nb_of_top_rows)
{
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
			if (dimy - Nodes[i][j].row_index <= nb_of_top_rows)
			{
				Nodes[i][j].fracture = true;
				Nodes[i][j].broken_in_x = true;
				Nodes[i][j].broken_in_y = true;
				//Nodes[i][j].ignore_fracture = true;
			} 
		}
	}
	
}

void Model::UpdateViscosity()
{
    REAL A = basic_settings.rock_viscosity_A;
    REAL E = basic_settings.rock_viscosity_E;
    REAL exponent = basic_settings.rock_viscosity_n;
    REAL V = basic_settings.rock_viscosity_V;
    REAL T;
    REAL diff_stress;

    Node *n;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy-1; j++)
        {
            n = &(Nodes[i][j]);

//            diff_stress = n->sy - n->sx;
            diff_stress = fabs((n->sy + n->sx) / 2.0 - n->P);

            T = n->temperature + 273;

            n->rock_viscosity = GetViscosity(T, diff_stress, A, exponent, E, V);

            if (n->rock_viscosity < 1e17)
                n->rock_viscosity = 1e17;
        }
    }
}

REAL Model::GetViscosity(REAL T, REAL diff_stress, REAL prefactor, REAL n, REAL activation_energy, REAL activation_volume)
{
    REAL val;

    val = prefactor * pow(diff_stress, 1.0 - n) * exp( (activation_energy + diff_stress*activation_volume) / (gas_constant * T)) ;

    if (isinf(val))
    {
        val = 1e25;
    }

    if (val > 1e25)
        val = 1e25;

    if (val < 1e17)
        val = 1e17;

    return (val);
}

void Model::SetGaussianMetamorphicFluidContentDistribution(REAL g_mean, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;


    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            node = &Nodes[i][j];

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        node->max_metamorphic_fluid *= p_rand ;
                        break;
                    }
                }
            }
            while (1);
        }
    }
}

void Model::SetFracRandomFactor(REAL g_mean, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;


    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            node = &Nodes[i][j];

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        node->frac_random_number = p_rand ;
                        break;
                    }
                }
            }
            while (1);
        }
    }
}

void Model::SetGaussianStrengthDistribution(REAL g_mean, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;


    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            node = &Nodes[i][j];

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        node->tensile_strength_x *= p_rand ;
                        node->tensile_strength_y *= p_rand ;

                        node->initial_tensile_strength_x *= p_rand ;
                        node->initial_tensile_strength_y *= p_rand ;

                        
                        break;
                    }
                }
            }
            while (1);
        }
    }
}

void Model::SetGaussianPorosityDistribution(REAL g_mean, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;


    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            node = &Nodes[i][j];

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        node->porosity *= p_rand ;
                        node->porosity_initial *= p_rand ;
                        break;
                    }
                }
            }
            while (1);
        }
    }
}

void Model::SetGaussianPermeabilityAndConductivityDistribution(REAL g_mean, REAL g_sigma)
{
    Node *node;
    REAL p_rand, prob, ran_nb;


    for (int i = 0; i < dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {

            node = &Nodes[i][j];

            do
            {
                p_rand = rand () / REAL(RAND_MAX);

                p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
                p_rand = p_rand + g_mean;

                prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
                prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
                prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

                ran_nb = rand () / REAL(RAND_MAX);

                if (ran_nb <= prob)
                {
                    if (p_rand <= 0.0)
                        ;
                    else
                    {
                        node->x_conductivity *= p_rand ;
                        node->y_conductivity *= p_rand ;
                        node->x_permeability *= p_rand ;
                        node->y_permeability *= p_rand ;

                        node->x_conductivity_initial *= p_rand ;
                        node->y_conductivity_initial *= p_rand ;
                        node->x_permeability_initial *= p_rand ;
                        node->y_permeability_initial *= p_rand ;

                        break;
                    }
                }
            }
            while (1);
        }
    }
}

REAL Model::CalcGaussianDistribution(REAL g_mean, REAL g_sigma)
{
    REAL p_rand, prob, ran_nb;

    REAL val = 1.0;

    do
    {
        p_rand = rand () / REAL(RAND_MAX);

        p_rand = (p_rand - 0.5) * 8.0 * (8.0 * g_sigma);
        p_rand = p_rand + g_mean;

        prob = (1 / (g_sigma * sqrt (2.0 * 3.1415927)));
        prob = prob * (exp (-0.5 * (((p_rand) - g_mean) / g_sigma) * (((p_rand) - g_mean) / g_sigma)));
        prob = prob * sqrt (2.0 * 3.1415927) * g_sigma;

        ran_nb = rand () / REAL(RAND_MAX);

        if (ran_nb <= prob)
        {
            if (p_rand <= 0.0)
                ;
            else
            {
                val *= p_rand ;
                break;
            }
        }
    }
    while (1);

    return (val);
}

// rate is in kg!!!
void Model::CalculateMetamorphicFluidReleaseFixedRateInKgPerSqrMtr(REAL min_depth, REAL max_depth, REAL annual_rate_per_square_metre)
{
   // this function needs to be _rewritten_ in order to include
    Node *node;
    REAL added_fluid_mass;

    REAL X_f;

    // charactersitic temperature interval for the reaction
    REAL T_min = 300;
    REAL T_max = 500;

    REAL fluid_max;

    REAL released_volume;
    REAL pore_volume;
    
    REAL bulk, rho0, fluid_mass_0, fluid_mass_1;
    
    REAL height_diff = fabs(max_depth - min_depth) / dy;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            node = &Nodes[i][j];

//            X_f = (node->max_metamorphic_fluid - node->released_metamorphic_fluid);
            X_f = node->max_metamorphic_fluid;

            fluid_max = node->volume * av_crustal_density * X_f;

			if (node->released_metamorphic_fluid < node->max_metamorphic_fluid)
			{
				if (node->depth > min_depth && node->depth < max_depth)
				{
					added_fluid_mass = annual_rate_per_square_metre / (dx * height_diff);
					node->released_metamorphic_fluid += added_fluid_mass / (node->volume * av_crustal_density);
					
					if (node->released_metamorphic_fluid  > node->max_metamorphic_fluid)
					{
						node->released_metamorphic_fluid = node->max_metamorphic_fluid;
						added_fluid_mass = 0.0;
					}
					
					// convert release fluid mass to excess pressure
					bulk = 1.0 / water_compressibility_base;
					rho0 = node->P * water_density / bulk; // here, P_0 = 0 and rho_0 is the common density
							
					released_volume = added_fluid_mass / water_density;
					pore_volume = node->porosity * node->volume;

					//node->P += released_volume / ((pore_volume + released_volume) * water_compressibility_base) ;

					fluid_mass_0 = rho0 * pore_volume;
					fluid_mass_1 = fluid_mass_0 + added_fluid_mass;
					
					node->P = bulk + node->P - bulk * fluid_mass_0 / fluid_mass_1;
					
					node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

					node->P_corrected = node->P - node->depth * g * water_density;
					node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
					node->head = node->P -node->depth*g*water_density;
				}
			}
        }
    }
}

void Model::CalculateMetamorphicFluidReleaseSimple(REAL min_depth, REAL max_depth, REAL rate_per_anno)
{
   // this function needs to be _rewritten_ in order to include
    Node *node;
    REAL added_fluid_mass;

    REAL X_f;

    // charactersitic temperature interval for the reaction
    REAL T_min = 300;
    REAL T_max = 500;

    REAL fluid_max;

    REAL released_volume;
    REAL pore_volume;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            node = &Nodes[i][j];

//            X_f = (node->max_metamorphic_fluid - node->released_metamorphic_fluid);
            X_f = node->max_metamorphic_fluid;

            fluid_max = node->volume * av_crustal_density * X_f;

            added_fluid_mass = 0.0;

			if (node->released_metamorphic_fluid < node->max_metamorphic_fluid)
			{
				if (node->depth > min_depth && node->depth < max_depth)
				{
					added_fluid_mass = fluid_max * dt/a * rate_per_anno;
					node->released_metamorphic_fluid += added_fluid_mass / (node->volume * av_crustal_density);
					
					if (node->released_metamorphic_fluid  > node->max_metamorphic_fluid)
					{
						node->released_metamorphic_fluid = node->max_metamorphic_fluid;
					}

					// convert release fluid mass to excess pressure
					released_volume = added_fluid_mass / water_density;
					pore_volume = node->porosity * node->volume;

					node->P += released_volume / ((pore_volume + released_volume) * water_compressibility_base) ;

					node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

					node->P_corrected = node->P - node->depth * g * water_density;
					node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
					node->head = node->P - node->depth*g*water_density;
				}
			}
        }
    }
}

void Model::AddFluidPressureIncrement(REAL inc, REAL ypos_upper, REAL ypos_lower)
{
	Node *node;
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
			node = &(Nodes[i][j]);
			
			if (ypos_upper > node->real_ypos &&  node->real_ypos > ypos_lower)
			{
				node->P += inc;

				node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

				node->P_corrected = node->P - node->depth * g * water_density;
				node->P_effective = (node->sx + node->sy)/2.0 - node->P;
				node->head = node->P - node->depth*g*water_density;
			}
		}
	}
}

void Model::SetEffectiveFluidPressureAtBottom(REAL value)
{
	Node *node;
	
	int upper_border = dimy / 10; // the lower 10 %
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=1; j<upper_border; j++)
		{
			node = &Nodes[i][j];
			
			node->P = (node->sy + node->sx)/2.0 - value;
			
			node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;			
			node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
			
			node->P_corrected = node->P - node->depth * g * water_density;
			node->head = -node->depth*g*water_density + node->P;

            node->preserve_pressure = true;
		}
	}
}

void Model::AddFluidPressureIncrementAtBottom(REAL inc)
{
	Node *node;
	
	int upper_border = dimy / 10; // the lower 10 %
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=1; j<upper_border; j++)
		{
			node = &(Nodes[i][j]);
			
			node->P += inc;

			node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

			node->P_corrected = node->P - node->depth * g * water_density;
			node->P_effective = (node->sx + node->sy)/2.0 - node->P;
			node->head = node->P - node->depth*g*water_density;
		}
	}
}

void Model::CalculateMetamorphicFluidRelease()
{
    // this function needs to be _rewritten_ in order to include
    Node *node;
    REAL added_fluid_mass;

    REAL X_f;

    // charactersitic temperature interval for the reaction
    REAL T_min = 300;
    REAL T_max = 500;

    REAL fluid_max;

    REAL released_volume;
    REAL pore_volume;

    // depends on the rate of subsidence
    // here, I assume 1 cm/a, with a temperature gradient of 30° C/km
    REAL temp_change_rate = 1.0 * 30.0 / (100.0*1000.0);

    // adapt to the timestep
    temp_change_rate = temp_change_rate * dt / (365.0*86400.0);
//    cout << temp_change_rate << endl;

//    temp_change_rate = 1e-9;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            node = &Nodes[i][j];

            X_f = node->max_metamorphic_fluid;
//            cout << X_f << endl;

            fluid_max = node->volume * av_crustal_density * X_f;

            added_fluid_mass = 0.0;

            if (node->released_metamorphic_fluid < X_f && node->temperature >= T_min)
            {
                added_fluid_mass = dt * temp_change_rate * (X_f*av_crustal_density) / (T_max - T_min) * node->volume;

                if ((node->released_metamorphic_fluid * node->volume * av_crustal_density) + added_fluid_mass > fluid_max)
                {
                    added_fluid_mass = (X_f * node->volume * av_crustal_density) - node->released_metamorphic_fluid;
                    node->released_metamorphic_fluid = X_f;
                }
                else
                {
                    node->released_metamorphic_fluid += added_fluid_mass / (node->volume * av_crustal_density);
//                    cout << node->released_metamorphic_fluid << " " << added_fluid_mass / (node->volume * av_crustal_density) << " " << X_f << endl;
                }

                // convert release fluid mass to excess pressure
                released_volume = added_fluid_mass / water_density;
                pore_volume = node->porosity * node->volume;

                node->P += released_volume / ((pore_volume + released_volume) * water_compressibility_base) ;

                node->P_total = (1.0 - node->porosity)*node->P_solid + node->porosity*node->P;

                node->P_corrected = node->P - node->depth * g * water_density;
                node->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - node->P;
                node->head = node->P - node->depth*g*water_density;
            }
        }
    }
}

void Model::CalculatePermeabilityAndConductivity()
{
    Node *nd;

    REAL x_frac_perm, x_frac_cond;
    REAL y_frac_perm, y_frac_cond;

    REAL x_bg_perm, x_bg_cond;
    REAL y_bg_perm, y_bg_cond;

    REAL x_mem_perm, x_mem_cond;
    REAL y_mem_perm, y_mem_cond;

    REAL fac1, fac2, fac3;	

    // CalcFractureAperture(bool close_fractures, bool use_crack_formula);    
    // CalcFractureAperture(false, true);

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            nd = &Nodes[i][j];
				
            CalculatePermeabilityAndConductivityFromPorosity(nd, x_bg_perm, x_bg_cond, y_bg_perm, y_bg_cond);
            CalculatePermeabilityAndConductivityFromFracture(nd, x_frac_perm, x_frac_cond, y_frac_perm, y_frac_cond);
            CalculatePermeabilityAndConductivityFromMemoryEffect(nd, x_mem_perm, x_mem_cond, y_mem_perm, y_mem_cond);

            fac1 = 1.0;
            nd->x_conductivity = fac1 * x_frac_cond + x_bg_cond + x_mem_cond;
            nd->x_permeability = fac1 * x_frac_perm + x_bg_perm + x_mem_perm;

            fac1 = 1.0;
            nd->y_conductivity = fac1 * y_frac_cond + y_bg_cond + y_mem_cond;
            nd->y_permeability = fac1 * y_frac_perm + y_bg_perm + y_mem_perm;
        }
    }
    
    REAL max_conductivity = 0.0;
    
    // now factor in the damage
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
			max_conductivity = 0.0;
			
            nd = &Nodes[i][j];
            
            if (nd->damage_zone_x)
            {
				if (NodeNeighbours[j*dimx + i][0])
				{
					max_conductivity = NodeNeighbours[j*dimx + i][0]->y_conductivity;
				
					if (NodeNeighbours[j*dimx + i][2])
					{
						if (NodeNeighbours[j*dimx + i][2]->y_conductivity > max_conductivity)
							max_conductivity = NodeNeighbours[j*dimx + i][2]->y_conductivity;
					}
				}
				else
				{
					max_conductivity = NodeNeighbours[j*dimx + i][2]->y_conductivity;
				}
					
				nd->y_conductivity = nd->y_conductivity + max_conductivity * basic_settings.fracking_prefactor;
			}
			
			max_conductivity = 0.0;
			
            if (nd->damage_zone_y)
            {
				if (NodeNeighbours[j*dimx + i][1])
				{
					max_conductivity = NodeNeighbours[j*dimx + i][1]->x_conductivity;
				
					if (NodeNeighbours[j*dimx + i][3])
					{
						if (NodeNeighbours[j*dimx + i][3]->x_conductivity > max_conductivity)
							max_conductivity = NodeNeighbours[j*dimx + i][3]->x_conductivity;
					}
				}
				else
				{
					max_conductivity = NodeNeighbours[j*dimx + i][3]->x_conductivity;
				}
					
				nd->x_conductivity = nd->x_conductivity + max_conductivity * basic_settings.fracking_prefactor;
			}
		}
    }
    
}

void Model::CalculatePorosityFromCompaction()
{
    REAL phi0, phi1, phi_min, c, eta, p_e;

//    for (int k=0; k<100; k++)
//    {
        for (int i=0; i<dimx; i++)
        {
            for (int j=0; j<dimy; j++)
            {
                eta = Nodes[i][j].rock_viscosity;
                c = 3.0/16.0;
                p_e = Nodes[i][j].P_effective;  // connolly

                phi0 = Nodes[i][j].porosity_last = Nodes[i][j].porosity;

                phi1 = exp(-c*dt*p_e/eta + log(phi0));
//                Nodes[i][j].porosity = exp(-c*dt/REAL(k)*p_e/eta + log(Nodes[i][j].porosity));

                phi_min = phi0*(1.0 - p_e / water_bulk_modulus);
                //phi_min = phi0 * ( p_e / water_bulk_modulus + 1.0 );

                if (phi1 < phi0)
                {
                    if (phi1 < phi_min)
                        Nodes[i][j].porosity = phi_min;
                    else
                        Nodes[i][j].porosity = phi1;
                }

                //if (Nodes[i][j].porosity < 0.0001)
                   //Nodes[i][j].porosity = 0.0001;

                //if (Nodes[i][j].porosity > 0.9)
                    //Nodes[i][j].porosity = 0.9;
            }
        }
//        CalculateNewFluidPressureFromPorsosity();
//    }
}

void Model::CalculateNewFluidPressureFromPorsosity()
{
    REAL phi0, phi1, dP, beta;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            phi0 = Nodes[i][j].porosity_last;
            phi1 = Nodes[i][j].porosity;
//            beta = Nodes[i][j].rock_compressibility;
//            beta = 1.0 / water_bulk_modulus;

//            dP = -(phi0 - phi1)/(beta * phi0);

//            Nodes[i][j].P = phi0*Nodes[i][j].P/phi1;
            Nodes[i][j].P -= water_bulk_modulus * (phi1 / phi0 - 1.0);

//            if (Nodes[i][j].P < 0.0)
//                cout << Nodes[i][j].P << endl;

            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity)*Nodes[i][j].P_solid + Nodes[i][j].porosity*Nodes[i][j].P;

            Nodes[i][j].P_corrected = Nodes[i][j].P - Nodes[i][j].depth * g * water_density;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;;

            Nodes[i][j].head = Nodes[i][j].P - Nodes[i][j].depth*g*water_density;
        }
    }
}

void Model::AddRandomBatches(REAL xmin, REAL xmax, REAL ymin, REAL ymax, REAL max_presseure_inc, REAL percentage)
{
    REAL ran1 = 0.0;		// calculate the amount of pressure
    REAL ran2 = 0.0;		// if smaller than 'percentage', add pressure to the node


    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            if (Nodes[i][j].xpos > xmin && Nodes[i][j].xpos < xmax)
            {
                if (Nodes[i][j].ypos > ymin && Nodes[i][j].ypos < ymax)
                {
                    Nodes[i][j].no_break = true;

                    // add pressure
                    ran1 = REAL(rand()) / REAL (RAND_MAX);
                    ran2 = REAL(rand()) / REAL (RAND_MAX);

                    if (ran2 < percentage)
                    {
                        cout  << "in" << endl;
                        Nodes[i][j].P += ran1 * max_presseure_inc;
                        Nodes[i][j].P_corrected = Nodes[i][j].P - Nodes[i][j].depth * g * water_density;
                        Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity)*Nodes[i][j].P_solid + Nodes[i][j].porosity*Nodes[i][j].P;
                        Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
                        Nodes[i][j].head = Nodes[i][j].P - Nodes[i][j].depth*g*water_density;
                    }

                    // calculate fluid content

                }
            }
        }
    }

    CalcSystemFluidMass();
}

void Model::CalculateFDParameters()
{
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            Nodes[i][j].CalculateFDParameters();
        }
    }
}

void Model::CalculateDiffusivity()
{
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            Nodes[i][j].CalculateDiffusivity();
        }
    }
}








void Model::CalculateCharacteristicTimeToFailure()
{
    REAL t_min = dt_initial;

    Node *n1, *n2;
    Node *node = NULL;
    Node *node2 = NULL;

    REAL t_node = 0.0;
    REAL K_eff;

    REAL background_perm, background_cond;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
			
            n1 = &(Nodes[i][j]);
            if (!n1->boundary)
            {
				// only if not yet broken in x
				if (!(n1->broken_in_y) && !(n1->no_break))
				{

					for (int k=1; k<4; k+=2)
					{
						n2 = NodeNeighbours[j*dimx + i][k];

						// only if flow towards the node
						if (n2)
						{
							if (n2->broken_in_y)
							{
								if (n2->head > n1->head)
								{
									if ( n1->sy - n2->P < n1->tensile_strength_y )
									{
										K_eff = 2.0 * n1->x_conductivity * n2->x_conductivity / (n1->x_conductivity + n2->x_conductivity);
										t_node = CalculateTimeToFailure(false, n1, n2, K_eff, dx);

										if (t_node < t_min && t_node > 0)
											t_min = t_node;
									}
								}
							}
						}
					}
				}

				// only if not yet broken in y
				if (!(n1->broken_in_x) && !(n1->no_break))
				{
					for (int k=0; k<3; k+=2)
					{
						n2 = NodeNeighbours[j*dimx + i][k];

						// only if flow towards the node
						if (n2)
						{
							if (n2->broken_in_x)
							{
								if (n2->head > n1->head)
								{
									if ( n1->sx - n2->P < n1->tensile_strength_x )
									{
										K_eff = 2.0 * n1->y_conductivity * n2->y_conductivity / (n1->y_conductivity + n2->y_conductivity);
										t_node = CalculateTimeToFailure(true, n1, n2, K_eff, dy);

										if (t_node < t_min && t_node > 0)
											t_min = t_node;
									}
								}
							}
						}
					}
				}
			}
        }
    }

    dt = t_min;
}

void Model::MarkDamageZones()
{
	Node *n1 = NULL;
	Node *n2 = NULL;
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{	
			n1 = &(Nodes[i][j]);
			n1->damage_zone_x = false;
			n1->damage_zone_y = false;
		}
	}
	
	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{	
			n1 = &(Nodes[i][j]);
			
			if (!n1->broken_in_x)
			{
				n2 = NodeNeighbours[j*dimx + i][0];
				if (n2)
				{
					if (n2->broken_in_x)
					{
						n1->damage_zone_x = true;
					}
				}
				
				n2 = NodeNeighbours[j*dimx + i][2];
				if (n2)
				{
					if (n2->broken_in_x)
					{
						n1->damage_zone_x = true;
					} 
				}
			}
			
			if (!n1->broken_in_y)
			{
				n2 = NodeNeighbours[j*dimx + i][1];
				if (n2)
				{
					if (n2->broken_in_y)
					{
						n1->damage_zone_y = true;
					}
				}
				
				n2 = NodeNeighbours[j*dimx + i][3];
				if (n2)
				{
					if (n2->broken_in_y)
					{
						n1->damage_zone_y = true;
					} 
				}
			}
		}
	}
}

void Model::MarkFailureCandidates()
{
    Node *n1, *n2;
    REAL t_node = 0.0;
    REAL K_eff;

    REAL background_perm, background_cond;
    
    // first, adjust the 

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            n1 = &(Nodes[i][j]);
            // only if not yet broken in x
            if (!(n1->broken_in_y) && !(n1->no_break))
            {
                for (int k=1; k<4; k+=2)
                {
                    n2 = NodeNeighbours[j*dimx + i][k];

                    // only if flow towards the node
                    if (n2)
                    {
                        if (n2->broken_in_y)
                        {
                            if (n2->head > n1->head)
                            {
                                if ( n1->sy - n2->P < n1->tensile_strength_y )
                                {
                                    K_eff = 2.0 * n1->x_conductivity * n2->x_conductivity / (n1->x_conductivity + n2->x_conductivity);
                                    t_node = CalculateTimeToFailure(false, n1, n2, K_eff, dx);

                                    if (t_node < dt && t_node > 0)
                                        n1->preliminary_broken_in_y = true;
                                }
                            }
                        }
                    }
                }
            }

            // only if not yet broken in y
            if (!(n1->broken_in_x) && !(n1->no_break))
            {
                for (int k=0; k<3; k+=2)
                {
                    n2 = NodeNeighbours[j*dimx + i][k];
                    // only if flow towards the node
                    if (n2)
                    {
                        if (n2->broken_in_x)
                        {
                            if (n2->head > n1->head)
                            {
                                if ( n1->sx - n2->P < n1->tensile_strength_x )
                                {
                                    K_eff = 2.0 * n1->y_conductivity * n2->y_conductivity / (n1->y_conductivity + n2->y_conductivity);
                                    t_node = CalculateTimeToFailure(true, n1, n2, K_eff, dy);

                                    if (t_node < dt && t_node > 0)
                                        n1->preliminary_broken_in_x = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

int Model::CheckFailureCandidates()
{
    Node *n1;

    int number_of_broken_bonds = 0;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            n1 = &(Nodes[i][j]);

            // only if not yet broken in x
            if (n1->preliminary_broken_in_y)
            {
                if ( n1->sy - n1->P < n1->tensile_strength_y )
                {
                    n1->fracture = 1;
                    n1->broken_in_y = true;
                    n1->permanent_damage_y = true;
                }
            }

            if (n1->preliminary_broken_in_x)
            {
                if ( n1->sx - n1->P < n1->tensile_strength_x )
                {
                    n1->fracture = 1;
                    n1->broken_in_x = true;
					n1->permanent_damage_x = true;
                }
            }
        }
    }

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            n1 = &(Nodes[i][j]);

            if (n1->preliminary_broken_in_x && n1->broken_in_x)
                number_of_broken_bonds++;

            if (n1->preliminary_broken_in_y && n1->broken_in_y)
                number_of_broken_bonds++;

            n1->preliminary_broken_in_x = false;
            n1->preliminary_broken_in_y = false;
        }
    }

    return(number_of_broken_bonds);
}

REAL Model::CalculateTimeToFailure(bool xfrac, Node *node, Node *node_nb, REAL K_eff, REAL dist)
{
    REAL head = node->head;
    REAL head_nb = node_nb->head;
    REAL depth = node->depth;
    REAL dt;
    REAL P_failure = 0.0;

    if (xfrac)
        P_failure = node->sx - node->tensile_strength_x;
    else
        P_failure = node->sy - node->tensile_strength_y;

    dist = dist*dist;

    dt = dist*P_failure + (-head-depth)*water_density*g*dist;
    dt /= (head_nb - head) * g * K_eff * water_density;

    return (dt);
}

void Model::SetFluidPressureCentral(int minx, int maxx, int miny, int maxy, REAL val)
{
    Node *nd;

    REAL phi;

    for (int i=miny; i<=maxy; i++)
    {
        for (int j=minx; j<=maxx; j++)
        {
            nd = &(Nodes[i][j]);

            phi = nd->porosity;

            nd->P_last = nd->P;
            nd->P = val;
            nd->P_corrected = Nodes[i][j].P - Nodes[i][j].depth * g * water_density;

            nd->P_total = (1.0-phi) * Nodes[i][j].P_solid + phi * Nodes[i][j].P;
            nd->P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;

            nd->head = Nodes[i][j].P - Nodes[i][j].depth*g*water_density;
        }
    }
}

void Model::AddFluidPressure(REAL increment)
{
    REAL phi;

    for (int j=30; j < 70; j++)
    {
        for (int i=30; i < 70; i++)
        {
            phi = Nodes[i][j].porosity;
            {
                Nodes[i][j].P += increment;
                Nodes[i][j].P_corrected = Nodes[i][j].P - Nodes[i][j].depth * g * water_density;
                Nodes[i][j].P_total = (1.0-phi) * Nodes[i][j].P_solid + phi * Nodes[i][j].P;
                Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;

				Nodes[i][j].head = Nodes[i][j].P - Nodes[i][j].depth*g*water_density;
            }
        }
    }
}

void Model::Nucleate()
{
    for (int i=0; i<dimx; i++)
    {
        for (int j=1; j<dimy; j++)
        {
            if (!Nodes[i][j].fracture)
            {
                if (!Nodes[i][j].broken_in_x)
                {
                    if (!Nodes[i][j].no_break)
                    {
                        if ((Nodes[i][j].sx - Nodes[i][j].P) + CalcTensileSeepageX(&(Nodes[i][j])) < Nodes[i][j].tensile_strength_x)
                        {
                            Nodes[i][j].broken_in_x = true;
                            Nodes[i][j].fracture = 1;
							Nodes[i][j].permanent_damage_x = true;
                            //                        cout << "fracture in x-direction" << endl;
                        }
                    }
                }

                if (!Nodes[i][j].broken_in_y)
                {
                    if (!Nodes[i][j].no_break)
                    {
                        if ((Nodes[i][j].sy - Nodes[i][j].P) + CalcTensileSeepageY(&(Nodes[i][j])) < Nodes[i][j].tensile_strength_y)
                        {
                            Nodes[i][j].broken_in_y = true;
                            Nodes[i][j].fracture = 1;
							Nodes[i][j].permanent_damage_y = true;
                            //                        cout << "fracture in y-direction" << endl;
                        }
                    }
                }
            }
        }
    }

    // in case of healing, the background-porosity needs to be reset
    CalculatePermeabilityAndConductivity();
}

void Model::BreakAll()
{
	for (int i=0; i<dimx; i++)
    {
        for (int j=1; j<dimy; j++)
        {
			Nodes[i][j].broken_in_x = true;
			Nodes[i][j].broken_in_y = true;
			Nodes[i][j].fracture = 1;
		}
	}
}

void Model::CalculatePermeabilityAndConductivityFromFracture(Node *nd, REAL& x_perm, REAL& x_cond, REAL& y_perm, REAL& y_cond)
{
    REAL fac = nd->frac_random_number;
    REAL fac2 = 1.0;

    // static values for now
    // switch (between 0 and said value) made with the "broken" and the "a" parameters
    if (nd->broken_in_y)
    {
		if (!basic_settings.fixed_fracture_conductivity)
			fac2 = nd->y_aperture / spacing;
		
		if (!basic_settings.damage_conductivity)
		{
			if (!basic_settings.use_cubic_law)
				x_cond = fac * fac2 * basic_settings.fracture_conductivity;
			else
				x_cond = fac2 * fac * nd->y_aperture * nd->y_aperture * water_density * g / (12.0 * nd->fluid_viscosity);
		}
		else 
		{
			x_cond = fac * fac2 * nd->x_damage * basic_settings.fracture_conductivity;
		}
		
		x_perm = x_cond * nd->fluid_viscosity / (water_density * g);
    }
    else
    {
        x_cond = 0.0;
        x_perm = 0.0;
    }

    if (nd->broken_in_x)
    {
		if (!basic_settings.fixed_fracture_conductivity)
			fac2 = nd->x_aperture / spacing;
		
		if (!basic_settings.damage_conductivity)
		{
			if (!basic_settings.use_cubic_law)
				y_cond = fac * fac2 * basic_settings.fracture_conductivity;
			else
				y_cond = fac2 * fac * nd->x_aperture * nd->x_aperture * water_density * g / (12.0 * nd->fluid_viscosity);
		}
		else
		{
			y_cond = fac * fac2 * nd->y_damage * basic_settings.fracture_conductivity;
		}
		
		y_perm = y_cond * nd->fluid_viscosity / (water_density * g);
    }
    else
    {
        y_cond = 0.0;
        y_perm = 0.0;
    }
}

void Model::CalculatePermeabilityAndConductivityFromPorosity(Node *nd, REAL& x_perm, REAL& x_cond, REAL& y_perm, REAL& y_cond)
{
    REAL c;			// proportionality constant, calculated from K_initial and phi_initial
    REAL phi0;
    REAL phi;

    phi0 = nd->porosity_initial;
    phi = nd->porosity;

    c = (phi/phi0) * (phi/phi0) * (phi/phi0);

    x_cond = nd->x_conductivity_initial * c;
    x_perm = nd->x_permeability_initial * c;

    y_cond = nd->y_conductivity_initial * c;
    y_perm = nd->y_permeability_initial * c;
}

void Model::CalculatePermeabilityAndConductivityFromMemoryEffect(Node *nd, REAL& x_mem_perm, REAL& x_mem_cond, REAL& y_mem_perm, REAL& y_mem_cond)
{
	if (nd->permanent_damage_y)
	{
		x_mem_cond = basic_settings.fracture_conductivity * basic_settings.memory_factor_conductivity;
		nd->tensile_strength_y = nd->initial_tensile_strength_y * basic_settings.memory_factor_tensile_strength;
	}
	else
		x_mem_cond = 0.0;
	
	if (nd->permanent_damage_x)
	{
		y_mem_cond = basic_settings.fracture_conductivity * basic_settings.memory_factor_conductivity;
		nd->tensile_strength_x = nd->initial_tensile_strength_x * basic_settings.memory_factor_tensile_strength;
	}
	else
		y_mem_cond = 0.0;

	x_mem_perm = x_mem_cond * water_viscosity / (water_density * g);    
	y_mem_perm = y_mem_cond * water_viscosity / (water_density * g);

}

void Model::CalcFluidDensity()
{
    REAL beta = 1.0 / water_bulk_modulus;
    REAL phi0;
    REAL phi1;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            phi0 = Nodes[i][j].porosity_last;
            phi1 = Nodes[i][j].porosity;
//            Nodes[i][j].fluid_density = water_density * (1.0 + Nodes[i][j].P * beta);

            //Nodes[i][j].fluid_density = Nodes[i][j].fluid_density * phi0 / phi1;
            Nodes[i][j].fluid_density = -Nodes[i][j].fluid_density * water_bulk_modulus / (Nodes[i][j].P - Nodes[i][j].P_last - water_bulk_modulus);
        }
    }
}

REAL Model::CalcSystemFluidMass()
{
    REAL total_fluid_mass = 0.0;
	REAL rho0;
	REAL pore_volume;
	REAL fluid_mass;
	
	Node *node;

	// convert release fluid mass to excess pressure
	REAL bulk = 1.0 / water_compressibility_base;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
			node = &(Nodes[i][j]);
			rho0 = - (water_density * bulk) / (node->P - bulk); // node->P * water_density / bulk, P_0 = 0 and rho_0 is the common density

			pore_volume = node->porosity * node->volume;				
			fluid_mass = rho0 * pore_volume;				      

            // thickness of all elements = 1.0 m, thus area == volume
            total_fluid_mass += fluid_mass;
        }
    }

   return (total_fluid_mass);
}

REAL Model::GetBottomLayerPosition()
{
    return(-bottom_layer_depth);
}

REAL Model::GetTopLayerPosition()
{
    return(-top_layer_depth);
}

void Model::DeleteSmallFractures(REAL threshold)
{
    Node *nd;

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimy; j++)
        {
            nd = &(Nodes[i][j]);

            if (nd->x_aperture / spacing < threshold)
            {
                nd->x_aperture = 0.0;
                nd->broken_in_x = 0.0;
            }

            if (nd->y_aperture / spacing < threshold)
            {
                nd->y_aperture = 0.0;
                nd->broken_in_y = 0.0;
            }

            if (!nd->broken_in_x && !nd->broken_in_y)
                nd->fracture = false;
            else
            {
                cout << nd->x_aperture << " " << nd->y_aperture << endl;
            }
        }
    }
}

REAL Model::CalcTensileSeepageX(Node* nd)
{
	// NodeNeighbours[j*dimx + i][0] = &Nodes[i][j-1];
	REAL tensile_seepage_force = 0.0;

	if (basic_settings.seepage)
	{	
		tensile_seepage_force = (NodeNeighbours[nd->row_index*dimx + nd->col_index][1]->P - nd->P) + (NodeNeighbours[nd->row_index*dimx + nd->col_index][3]->P - nd->P);

		if (tensile_seepage_force > 0.0)
			tensile_seepage_force = 0.0;
	}
	
	return (tensile_seepage_force);
}

REAL Model::CalcTensileSeepageY(Node *nd)
{
	// NodeNeighbours[j*dimx + i][0] = &Nodes[i][j-1];
	REAL tensile_seepage_force = 0.0;
	
	if (basic_settings.seepage)
	{
		if (nd->top_boundary)
			tensile_seepage_force = (NodeNeighbours[nd->row_index*dimx + nd->col_index][0]->P - nd->P);
		else if (nd->bottom_boundary)
			tensile_seepage_force = (NodeNeighbours[nd->row_index*dimx + nd->col_index][2]->P - nd->P);
		else
			tensile_seepage_force = (NodeNeighbours[nd->row_index*dimx + nd->col_index][0]->P - nd->P) + (NodeNeighbours[nd->row_index*dimx + nd->col_index][2]->P - nd->P);
		
		if (tensile_seepage_force > 0.0)
			tensile_seepage_force = 0.0;
	}
		
	return (tensile_seepage_force);
}
