#ifndef MODEL_H
#define MODEL_H

#include "mainwindow.h"
#include "definitions.h"
#include "node.h"
#include "fractures.h"
#include "omp.h"
#include "basicsettings.h"
#include "statevariables.h"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include <vector>

class MainWindow;
class Fractures;

class Model
{
public:
    Model(int argc = 1);

    int dimx;
    int dimy;
    int total_timesteps;
    
    REAL spacing;

    REAL total_time, dt, dt_initial, dx, dy;
    REAL time_passed;

    int length_in_x, length_in_y;

    void RunMainLoop(MainWindow *W);

    Node **Nodes;

    void SaveModel();
    void OpenModel(MainWindow *W, QString filename = "");
    void ChangeSaveInterval(int interval);

    int save_number;    // number of a saved model-file

	void CalculatePermeabilityAndConductivityFromMemoryEffect(Node *nd, REAL& x_mem_perm, REAL& x_mem_cond, REAL& y_mem_perm, REAL& y_mem_cond);

    REAL GetFluidPressure(int i, int j);
    REAL GetOverPressure(int i, int j);
    REAL GetSolidPressure(int i, int j);
    REAL GetEffectivePressure(int i, int j);
    REAL GetDiffusivityInX(int i, int j);
    REAL GetDiffusivityInY(int i, int j);
    REAL GetSx(int i, int j);
    REAL GetSy(int i, int j);
    REAL GetCorrectedFluidPressure(int i, int j);
    REAL GetHydraulicHead(int i, int j);
    REAL GetConductivityInX(int i, int j);
    REAL GetConductivityInY(int i, int j);
    REAL GetViscosity(int i, int j);
    REAL GetTemperature(int i, int j);
    REAL GetPorosity(int i, int j);
    REAL GetTotalPressure(int i, int j);
    REAL GetFluidMass(int i, int j);
    int GetFractureStatus(int i, int j);
    bool GetBrokenInX(int i, int j);
    bool GetBrokenInY(int i, int j);
	REAL GetXDamage(int i, int j);
	REAL GetYDamage(int i, int j);
	REAL GetTensileStrengthX(int i, int j);
	REAL GetTensileStrengthY(int i, int j);
	
	void MarkDamageZones();
	
	REAL CalcTensileSeepageX(Node* nd);
	REAL CalcTensileSeepageY(Node* nd);

    REAL GetXApertureRatio(int i, int j);
    REAL GetYApertureRatio(int i, int j);

    REAL GetBottomLayerPosition();
    REAL GetTopLayerPosition();

    Node* GetNodeNeighbour(int nodeposition, int neighbourposition);
    int GetFractureNumber(int i, int j);

    int GetFractureActivity(bool active, int i, int j);

    int GetTimestep();

    StateVariables state_variables;
    BasicSettings basic_settings;

    QString model_file;

private:
    // control the simulation
    void InitSimulation();
    void Simulation(MainWindow *W);

    bool Simulate();
    
    void BreakAll();
    
    void SetEffectiveFluidPressureAtBottom(REAL value);
    void AddFluidPressureIncrementAtBottom(REAL inc);
    
    void AddNormalDistributedFluidPressureIncrement(REAL inc, REAL g_sigma);
    
    void AddNormalDistributedFluidAtBottom(REAL total_amount_per_anno, REAL g_sigma);
    
    void AddFluidPressureIncrement(REAL inc, REAL ypos_upper, REAL ypos_lower);
    
    void SetConstantOverPressureBottomRow(REAL value);
    
    void SetConductivityGradient(REAL top_cond, REAL bottom_cond);
    
    void CalculateMetamorphicFluidReleaseSimple(REAL min_depth, REAL max_depth, REAL rate_per_anno);
    void CalculateMetamorphicFluidReleaseFixedRateInKgPerSqrMtr(REAL min_depth, REAL max_depth, REAL annual_rate_per_square_metre);

    void AdjustChangeOfHorizontalStressToFluidPressure();
    void CalculateHorizontalStressFromFluidPressure();
    
    void AddNormalDistributedFluid(REAL fluid_release_mass_per_anno_per_square_meter, REAL g_sigma, REAL top, REAL bottom);

	void SetTopBoundariesAsPermanentFaults(int nb_of_top_rows);


	REAL CalculateTimeTillPropagation();
	void BreakNeighbours();

	// save to file
	void InitCorrectedFluidPressureFile();
	void InitEffectivePressureFile();
	void InitFluidPressureFile();
	void DumpFluidPressure();
	void DumpCorrectedFluidPressure();
	void DumpEffectivePressure();
	void InitFlowrateFile();
	void DumpFlowrate();

    // subsidence
    void Lift();

    void UpdateModelDimensions();
    void UpdateSolidStress();
    void UpdateTemperature();
    void UpdateViscosity();

    // solver
    void SolvePressureADI();
    void DarcyFlow();
    void SolvePressureADIHeterogeneous();

    void Zip (QString filename , QString zipfilename);
    void Unzip (QString zipfilename , QString filename);

    // functions
    void SetGaussianPorosityDistribution(REAL g_mean, REAL g_sigma);
    void SetGaussianMetamorphicFluidContentDistribution(REAL g_mean, REAL g_sigma);
    void SetGaussianPermeabilityAndConductivityDistribution(REAL g_mean, REAL g_sigma);
    REAL CalcGaussianDistribution(REAL g_mean, REAL g_sigma);
    void SetGaussianStrengthDistribution(REAL g_mean, REAL g_sigma);
    void SetFracRandomFactor(REAL g_mean, REAL g_sigma);

    void CalculateDiffusivity();
    void CalculateFDParameters();
    void AddFluidPressure(REAL increment);
    void SetFluidPressureCentral(int minx, int maxx, int miny, int maxy, REAL val);

    void Nucleate();
    void CalculateCharacteristicTimeToFailure();
    void MarkFailureCandidates();
    int CheckFailureCandidates();
    REAL CalculateTimeToFailure(bool xfrac, Node *node, Node *node_nb, REAL K_eff, REAL dist);
    void CalcFractureAperture(bool close_fractures, bool use_crack_formula);
    void SealFractures();
    
    int CountFracturedNodes();

    REAL CalcSystemFluidMass();
    void CalcFluidDensity();

    void CalculateNewFluidPressureFromPorsosity();

    void CalculatePorosityFromCompaction();

    void CalculatePermeabilityAndConductivity();
    void CalculatePermeabilityAndConductivityFromPorosity(Node *nd, REAL& x_perm, REAL& x_cond, REAL& y_perm, REAL& y_cond);
    void CalculatePermeabilityAndConductivityFromFracture(Node *nd, REAL& x_perm, REAL& x_cond, REAL& y_perm, REAL& y_cond);

    void CalculateMetamorphicFluidRelease();

    void DeleteSmallFractures(REAL threshold);

    void AddRandomBatches(REAL xmin, REAL xmax, REAL ymin, REAL ymax, REAL max_presseure_inc, REAL percentage);
    void AddNormalDistributedRandomFluidBatches(REAL total_amount_per_second, REAL g_sigma);

    REAL GetViscosity(REAL T, REAL diff_stress, REAL prefactor, REAL n, REAL activation_energy, REAL activation_volume);

	void SaveStatistics();
    void InitFractureRatioFile();
    void InitOutflowFile();
    void SaveFractureRatio();
    void SaveOutflow();

    // the position of the profile
    REAL top_layer_depth;
    REAL bottom_layer_depth;
    REAL profile_height;

    // the rate of subsidence
    REAL rate_of_subsidence;

    int timestep;
    int saved_timestep;
    int last_timestep_of_previous_loop;
    bool running;

    bool stop_adding_water ;

    REAL total_fluid_mass;
    REAL fluid_mass_previous, fluid_mass;
    REAL fluid_mass_loss;
    REAL integrated_flux;
    
    REAL fluid_mass_lost;

    // for the explicit monte-carlo solution
    std::vector < Node* > ran;
    Node **NodeList;
    Node ***NodeNeighbours;


    Node *broken_in_y_pointer_1;
    Node *broken_in_y_pointer_2;
    Node *broken_in_x_pointer_1;
    Node *broken_in_x_pointer_2;

    Fractures *fractures;

};

#endif // MODEL_H
