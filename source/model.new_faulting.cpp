
#include "model.h"

using namespace std;

void Model::BreakNeighbours()
{
	Node *n1;
	Node *n2;

	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
            n1 = &(Nodes[i][j]);

			// only if not yet broken in y
			if (!(n1->broken_in_y) && !(n1->no_break))
			{

				for (int k=1; k<4; k+=2)
				{
					n2 = NodeNeighbours[j*dimx + i][k];

					if (n2)
					{
						if (n2->broken_in_y)
						{
							if (n2->sy - n2->P < 0)
							{
								if ( n1->sy - n2->P < n1->tensile_strength_y )
								{
									n1->fracture = true;
									n1->broken_in_y = true;
								}
							}
						}

					}
				}
            }

            // only if not yet broken in x
            if (!(n1->broken_in_x) && !(n1->no_break))
            {

                for (int k=0; k<3; k+=2)
                {
                    n2 = NodeNeighbours[j*dimx + i][k];

                    if (n2)
                    {
                        if (n2->broken_in_x)
                        {
                            if (n2->sx - n2->P < 0)
                            {
                                if ( n1->sx - n2->P < n1->tensile_strength_x )
                                {
                                    n1->fracture = true;
                                    n1->broken_in_x = true;
                                }
                            }
                        }
                    }
                }
            }
        }
	}

    // the lines below will usually CLOSE new fractures, because no flow has been taken place yet. Better comment them out.
    //CalcFractureAperture(false, true);
    //CalculatePermeabilityAndConductivity();
}

REAL Model::CalculateTimeTillPropagation()
{
	REAL h, oh, oh1, oh2, oh3, oh4;
	REAL Keff1, Keff2, Keff3, Keff4;
	REAL t;

	REAL requiredP;

	REAL tmin = dt_initial;

	Node *nd;
	Node *nd2;

	bool cont = false;

	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
			nd = &(Nodes[i][j]);

			if (nd->fracture && !nd->no_break)
			{
				if (nd->sx - nd->P < 0.0 || nd->sy - nd->P < 0.0) 											//	if there is aperture
				{
					oh = nd->head;

					if (NodeNeighbours[j*dimx + i][0])
					{
						oh1 = NodeNeighbours[j*dimx + i][0]->head;
						Keff1 = ( 2.0 * nd->y_conductivity * NodeNeighbours[j*dimx + i][0]->y_conductivity ) / ( nd->y_conductivity + NodeNeighbours[j*dimx + i][0]->y_conductivity );
					}
					else
					{
						oh1 = 0;
						Keff1 = 0.0;
					}

					if (NodeNeighbours[j*dimx + i][1])
					{
						oh2 = NodeNeighbours[j*dimx + i][1]->head;
						Keff2 = ( 2.0 * nd->x_conductivity * NodeNeighbours[j*dimx + i][1]->x_conductivity ) / ( nd->x_conductivity + NodeNeighbours[j*dimx + i][1]->x_conductivity );
					}
					else
					{
						oh2 = 0;
						Keff2 = 0.0;
					}

					if (NodeNeighbours[j*dimx + i][2])
					{
						oh3 = NodeNeighbours[j*dimx + i][2]->head;
						Keff3 = ( 2.0 * nd->y_conductivity * NodeNeighbours[j*dimx + i][2]->y_conductivity ) / ( nd->y_conductivity + NodeNeighbours[j*dimx + i][2]->y_conductivity );
					}
					else
					{
						oh3 = 0;
						Keff3 = nd->y_conductivity;
					}

					if (NodeNeighbours[j*dimx + i][3])
					{
						oh4 = NodeNeighbours[j*dimx + i][3]->head;
						Keff4 = ( 2.0 * nd->x_conductivity * NodeNeighbours[j*dimx + i][3]->x_conductivity ) / ( nd->x_conductivity + NodeNeighbours[j*dimx + i][3]->x_conductivity );
					}
					else
					{
						oh4 = 0;
						Keff4 = 0.0;
					}

					for (int k=0; k<4; k++)
					{

						nd2 = NodeNeighbours[j*dimx + i][k];

						cont = false;

						if (nd2)
						{
							if (k+1 % 2 == 0)
							{
                                cont = !nd2->broken_in_y;
							}
							else
							{
                                cont = !nd2->broken_in_x;
							}
						}

						if (cont)
						{
							if (k+1 % 2 == 0)
							{
                                requiredP = nd2->tensile_strength_y + nd->sy;
							}
							else
							{
                                requiredP = nd2->tensile_strength_x + nd->sx;
							}

                            h = requiredP + nd->depth * water_density * g;

                            t = - (((4*oh-h)*spacing*spacing) / (Keff4*oh4 + Keff3*oh3 + Keff2*oh2 + Keff1*oh1 + (-Keff4 - Keff3 - Keff2 - Keff1)*oh));

							if (t > 0.0)
                            {
								if (t < tmin)
                                {
									tmin = t;
                                }
                            }
						}
					}
				}
			}
		}
	}


	return (tmin);
}

int Model::CountFracturedNodes()
{
	int counter = 0;

	for (int i=0; i<dimx; i++)
	{
		for (int j=0; j<dimy; j++)
		{
			if (Nodes[i][j].fracture)
				counter++;
		}
	}

	return (counter);
}
