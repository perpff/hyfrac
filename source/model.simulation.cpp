#include "model.h"

using namespace std;

void Model::InitSimulation()
{
    // AddFluidPressure(1);
    // AddRandomBatches(0.0, 1.0, 0.0, 0.2, 1e7 * dt / dt_initial, 0.03);
    // AddFluidPressure(1e7);
    
    REAL buffer;

    state_variables.fluid_mass = CalcSystemFluidMass();

    srand(time(0));

    SetFracRandomFactor(1.0, 0.05);
//    SetGaussianMetamorphicFluidContentDistribution(1.0, 0.1);
//    SetGaussianPorosityDistribution(1.0, 0.005);
//    SetGaussianPermeabilityAndConductivityDistribution(1.0, 0.05);
    SetGaussianStrengthDistribution(1.0, 0.1);

   // SetFluidPressureCentral(20, 30, 20, 30, 9.9e6);

   	CalculatePermeabilityAndConductivity();

    int cycle = 1;

   	cout << "Initial loops: " << cycle << endl;

	// init output files
   	InitFractureRatioFile();
   	InitOutflowFile();
   	InitCorrectedFluidPressureFile();
   	InitEffectivePressureFile();
   	InitFluidPressureFile();
   	InitFlowrateFile();

	buffer = dt;
	dt = 10*a;
   
   //SetConductivityGradient(1e-9, 1e-5);
   REAL yearly_fluid_volume = 1000, sigma = 0.1;

	BreakAll();

   for (int i=0; i < cycle; i++)
   {
       //UpdateViscosity();

       //~ CalculatePorosityFromCompaction();
       //~ CalculatePermeabilityAndConductivity();
       //~ CalculateNewFluidPressureFromPorsosity();

       CalcSystemFluidMass();
      // AddNormalDistributedFluid(10, 0.00001, 0, 16000);
	
       CalculatePermeabilityAndConductivity();
       DarcyFlow();
       CalcFractureAperture(false, true);
       
		//~ SaveModel();
//~ 
		//~ save_number++;

       cout << "Loop " << i << " of " << cycle << " initial loops." << endl;
   }
   
   //SetTopBoundariesAsPermanentFaults(2);

   dt = dt_initial;
   
   // SetEffectiveFluidPressureAtBottom(-9.9e5);
   // SetConstantOverPressureBottomRow(-1.0000001e6);
   SetConstantOverPressureBottomRow(-1.1e6);
}

void Model::Simulation(MainWindow *W)
{

    int number_of_broken_bonds = 0;
    int number_of_broken_bonds_buffer = 0;
    int count_changes = 0;
    bool something_broke = false;
    REAL buffer;
	REAL time_passed_in_this_timestep = 0.0;

    // time-independent failure
    dt = CalculateTimeTillPropagation();

	cout << dt << endl;

    time_passed += dt_initial;

    // start the time-dependent part
    if (dt < dt_initial && dt > 0.0)
    {
		cout << dt << endl;
        int counter = int (dt_initial / dt) + 1;
        dt = dt_initial / REAL(counter);

        cout << "Internal timestep size: " << dt << " Global timestep size: " << dt_initial << endl;
		
		time_passed_in_this_timestep = 0.0;
		
        for (int i = 0; i <  counter; i++)
        {
            cout << "internal loop:  step " << i << " out of " << counter << endl;

            buffer = dt;
            dt = CalculateTimeTillPropagation();

            if (dt > buffer * (counter - i))
            {
                // go to the next timestep
                Simulate();
				break;
			}
			else
			{
                something_broke = Simulate();

                // see if a larger dynamic timestep is more appropriate
				if (dt > buffer)
				{
					buffer = buffer * REAL (counter - i);	// restzeit
					counter = int (buffer/dt) + 1;				// neuer counter 
					dt = buffer / REAL(counter);
					i=0;
				}
				else
					dt = buffer;
			}
 			
            if (something_broke)
 			{
				W->SetTotaltimeLabel(REAL((time_passed) / (365*86400)));
				W->Replot();
			}

            if (basic_settings.count_fractures_to_save_file)
            {
                if (basic_settings.SaveInterval != 0)
                {
                    if (something_broke)
                    {
                        if (count_changes % basic_settings.SaveInterval == 0)
                        {
                            SaveModel();
                            if (basic_settings.save_png)
                                W->save_png();
                            save_number++;
                            count_changes = 0;
                        }
                    }
                    else
                        count_changes++;
                }
            }

            if (basic_settings.continuous_signal)
            {
                if (something_broke)
                {
					SaveStatistics();
                }
            }
        }
    }
    else
    {
        Simulate();
    }
}

// flow, break and heal
bool Model::Simulate()
{
    int number_of_broken_bonds;
    bool something_broke;

    REAL yearly_fluid_volume = 100, sigma = 0.1, bottom = 16000, top = 0;

    //~ CalculatePorosityFromCompaction();
    //~ CalculateNewFluidPressureFromPorsosity();
    //~ CalculatePermeabilityAndConductivity();

    fractures->UpdateFractureList();

    // CalculateMetamorphicFluidReleaseSimple(12000.0, 15000.0, 0.000001);
    // rate is in kg / m^2!!!
    // CalculateMetamorphicFluidReleaseFixedRateInKgPerSqrMtr(10000.0, 12000.0, 5.0);
    // AddNormalDistributedFluid(yearly_fluid_volume, sigma, top, bottom);
    // AddNormalDistributedFluidAtBottom(yearly_fluid_volume, sigma);
    // AddFluidPressureIncrement(30000, -10000, -12000); // inc in Pascal, depth is the "real_ypos"-parameter
    // AddFluidPressureIncrementAtBottom(1e3 * dt / dt_initial); // adds fluid pressure to the lower 10% of the model
	//AddNormalDistributedFluidPressureIncrement(1e3 * dt / dt_initial, 0.1);
	
	//AddNormalDistributedFluid(REAL fluid_release_mass_per_anno_per_square_meter, REAL g_sigma, REAL top, REAL bottom)
	//~ AddNormalDistributedFluid(3000.0, 0.1, 14400, 16000);
	
    number_of_broken_bonds = CountFracturedNodes();
    Nucleate();
    BreakNeighbours();
    CalculatePermeabilityAndConductivity();
    DarcyFlow();
    CalculatePermeabilityAndConductivity();
    CalcFractureAperture(false, true);
    BreakNeighbours();
    CalculatePermeabilityAndConductivity();

    if (number_of_broken_bonds != CountFracturedNodes())
        something_broke = true;
    else
        something_broke = false;

    //UpdateViscosity();
    SealFractures();

    //~ CalculatePorosityFromCompaction();
    //~ CalculatePermeabilityAndConductivity();
    //~ CalculateNewFluidPressureFromPorsosity();

    CalcSystemFluidMass();

    Lift();

    return (something_broke);
}
