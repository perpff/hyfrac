#include "model.h"

using namespace std;

//***********************************************************************************************************
// this is a MC solver, based on an explicit solution Not fast, but applicable to heterogeneous materials.
//***********************************************************************************************************

void Model::DarcyFlow()
{	
    // Variable to find a random neighbour of each Unode
    int RandomNB;

    int number_of_nodes = dimx * dimy;

    srand(time(NULL));


    // NOTE ABOUT UNODE NEIGHBOURS ID:
    //
    //		2
    //	3 Unode 1
    //		0

    REAL KNode, KNodeNB;							// Variables for storing hydraulic conductivity (m/s) of each unode pair
    REAL K_eff; 									// Variable to store effective hydraulic conductivity (m/s) of the segment between two neighbour unodes
    REAL CFL; 										// Variables to calculate the Courant-Friedrich-Levy condition for explicit FD scheme for each Unode pair
    REAL tdelta_dummy; 								// Dummy variable to calcualte new tdelta, in case CFL condition is not satisfied
    REAL tdeltaFlow = dt; 							// If there is no problem with CFL condition, we get tdeltaFlow = tdelta defined at the settings file

    // Find real distance between two neighbour Unodes
    REAL Distance = spacing;

    // Loop to calculate effective hydraulic conductivities for each segment between two neighbour Unodes and calculate proper time step size to satisfy CFL condition
    for (int i=0;i<dimx;i++)
    {

        for (int j=0; j<dimy; j++)
        {

            if ( Nodes[i][j].x_conductivity > Nodes[i][j].y_conductivity )
                KNode = Nodes[i][j].x_conductivity;
            else
                KNode = Nodes[i][j].y_conductivity;

            // Calculate effective hydraulic conductivity for the four neighbours, and check whether CFL is fine

            for (int k=0; k<4; k++)
            {
                if ( NodeNeighbours[j*dimx+i][k] )
                {
                    if ( NodeNeighbours[j*dimx+i][k]->x_conductivity > NodeNeighbours[j*dimx+i][k]->y_conductivity)
                        KNodeNB = NodeNeighbours[j*dimx+i][k]->x_conductivity;
                    else
                        KNodeNB = NodeNeighbours[j*dimx+i][k]->y_conductivity;

                    K_eff = ( 2.0 * KNode * KNodeNB ) / ( KNode + KNodeNB ); 	// Calculate effective k*

                    CFL = ( tdeltaFlow * K_eff ) / (Distance*Distance) ; 			// Calculate CFL
                    if ( CFL >= 0.5 )
                    {
                        tdelta_dummy = ( (Distance*Distance) * 0.49 ) / ( K_eff );
                        if ( tdelta_dummy < tdeltaFlow )
                            tdeltaFlow = tdelta_dummy;
                    }
                }
            }
        }
    }

    int NumberOfFlowTimeSteps = dt / tdeltaFlow;

    if ( NumberOfFlowTimeSteps <= 50 )
    {
//        cout << endl << "Warning, we have increased the number of time steps for fluid flow to 50" << endl;
//        cout << "by reducing the time step size. This is done to increase precision." << endl;
        tdeltaFlow = dt / 50;
        NumberOfFlowTimeSteps = 50;
    }

//    cout << endl << "tdelta = " << dt << endl;
//    cout << "tdeltaFlow = " << tdeltaFlow << endl;
//    cout << "NumberOfFlowTimeSteps = " << NumberOfFlowTimeSteps << endl;
//    cout << "Calculation time= " << NumberOfFlowTimeSteps * tdeltaFlow << endl << endl;

    REAL OldHead, OldHeadNB;
    Node *Node1, *Node2;
    int size;

    // to prevent updating a single node always at the same time, we shuffle them randomly at each step
    ran.clear();

    for ( int j = 0; j < number_of_nodes; j++ )
        ran.push_back( NodeList[j] );

    size = ran.size();

    cout << "Number of flow-timesteps: " << NumberOfFlowTimeSteps << endl;
    
    bool surface = false;

    for ( int s = 1; s < NumberOfFlowTimeSteps+1; s++ )
    {
//        if (s%100 == 0)
//            cout << s << " of " << NumberOfFlowTimeSteps << " flowtimesteps" << endl;

        for ( int l = 1; l < 3; l++ ) // We have to do the loop twice if the unode grid is rectangular, to ensure that there are on average four calculations per node per time step (one per segment)
        {
            random_shuffle( ran.begin(), ran.end() );

            // that should keep the program responsive, even if we have very long calculation times
            QCoreApplication::processEvents();

            // it's significantly faster if it's *NOT* parallelized!!!
//#pragma omp parallel private(Node1,Node2,K_eff,KNode,KNodeNB,OldHead,OldHeadNB,RandomNB)
            {
                for (int m=0; m<size; m++)
                {
                    RandomNB = rand() % 4;

                    Node1 = ran.at(m);

                    Node2 = NodeNeighbours[Node1->row_index*dimx + Node1->col_index][RandomNB];

					surface = false;
					
						if (RandomNB == 0)
						{
							KNode = Node1->y_conductivity;
							OldHead = Node1->head;

							if (Node2)
							{
								KNodeNB = Node2->y_conductivity;
								OldHeadNB = Node2->head;
							}
							else
							{
								KNodeNB = 0.0;
								OldHeadNB = Node1->head;
							}
							
						}
						else if (RandomNB == 2)
						{
							KNode = KNodeNB = Node1->y_conductivity;
							OldHead = Node1->head;

							if (Node2)
							{
									KNodeNB = Node2->y_conductivity;
									OldHeadNB = Node2->head;
							}
							else // this is the surface, rivers etc!
							{
								KNodeNB = Node1->y_conductivity;
								OldHeadNB = 0.0;
								surface = true;
							}
						}
						else
						{
							KNode = Node1->y_conductivity;
							OldHead = Node1->head;

							if (Node2)
							{
								KNodeNB = Node2->y_conductivity;
								OldHeadNB = Node2->head;
							}
						}

						if (KNode != 0.0 && KNodeNB != 0.0)
							K_eff = ( 2.0 * KNode * KNodeNB ) / ( KNode + KNodeNB ); // Calculate effective hydraulic conductivity (m/s)
						else
							K_eff = 0.0;

                        // #pragma omp critical
						{
                            if (!Node1->preserve_pressure)
                            {
                                Node1->head = OldHead - ( ( (K_eff) * ( (OldHead - OldHeadNB) / (Distance) ) ) / ( (Distance) )  ) * tdeltaFlow; // Calculate new head unode 1 (m)
                                if (Node1->head < 1e-10)
                                    Node1->head = 0.0;
                                 
                                    
                                Node1->P_last = Node1->P;
                                Node1->P = Node1->head + Node1->depth * water_density * g ; // Calculate new fluid pressure at unode 1 (Pa)
                                
                               if (surface)
                                  fluid_mass_lost = fluid_mass_lost + water_density/(1.0 - Node1->P*water_compressibility_base) - water_density/(1.0 - Node1->P_last*water_compressibility_base);
                                
                                Node1->P_corrected = Node1->P - water_density * g * Node1->depth;
                                Node1->P_total = (1.0 - Node1->porosity)*Node1->P_solid + Node1->porosity*Node1->P;
                                Node1->P_effective = (Node1->sx + Node1->sy)/2.0 - Node1->P;
                                
                                
                            }

                            if (Node2)
                            {
                                if (!Node2->preserve_pressure)
                                {
                                    Node2->head = OldHeadNB + (OldHead - Node1->head); // Calculate new head unode 2 (m)

                                    if (Node2->head < 1e-10)
                                        Node2->head = 0.0;

                                    Node2->P_last = Node2->P;
                                    Node2->P = Node2->head + Node2->depth * water_density * g; // Calculate new fluid pressure at unode 2 (Pa)
                                    Node2->P_corrected = Node2->P - water_density * g * Node2->depth;
                                    Node2->P_total = (1.0 - Node2->porosity)*Node2->P_solid + Node2->porosity*Node2->P;
                                    Node2->P_effective = (Node2->sx + Node2->sy)/2.0 - Node2->P;
                                }
                            }
						}

						if (isnan(Node1->P))
						{
							cout << "catched isnan" << endl;
						}
						
				}
            }
        }
    }
}



/******************************************************************************
 * This is an ADI-solver
 * it's mandatory that this thing considers periodic boundaries in X-direction
 *
 * !!!! This needs still to be implemented !!!!
 *
 * In the future, this algorithm can be used to calculate the temperature distribution.
 * ****************************************************************************/
void Model::SolvePressureADI()
{

    if (dimx != dimy)
    {
        cout << "If you want to use the ADI solver, the system needs to be of square dimensions" << endl;
        exit(0);
    }

    int dim = dimx+2;

    REAL a[dim], b[dim], c[dim], d[dim], c2[dim], d2[dim];
    REAL BUFFER[dim][dim];
    REAL ALPHA[dim][dim];
    REAL BETA[dim][dim];
    REAL P[dim][dim];

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            ALPHA[i+1][j+1] = Nodes[i][j].alpha;
            BETA[i+1][j+1] = Nodes[i][j].beta;
            P[i+1][j+1] = Nodes[i][j].P_corrected;
        }
    }

    for (int i=0;i<dimx;i++)
    {
        ALPHA[0][i+1] = Nodes[dimx-1][i].alpha;
        BETA[0][i+1] = Nodes[dimx-1][i].beta;
        P[0][i+1] = Nodes[dimx-1][i].P_corrected;

        ALPHA[dim-1][i+1] = Nodes[0][i].alpha;
        BETA[dim-1][i+1] = Nodes[0][i].beta;
        P[dim-1][i+1] = Nodes[0][i].P_corrected;

        ALPHA[i+1][0] = Nodes[i][dimx-1].alpha;
        BETA[i+1][0] = Nodes[i][dimx-1].beta;
        P[i+1][0] = Nodes[i][0].P_corrected;

        ALPHA[i+1][dim-1] = Nodes[i][0].alpha;
        BETA[i+1][dim-1] = Nodes[i][0].beta;
        P[i+1][dim-1] = Nodes[i][dimx-1].P_corrected;
    }


    // first part of the calculation, x-sweep
#pragma omp parallel for shared(BUFFER) private(a,b,c,d,c2,d2)
    for (int j=1; j<dim-1; j++)
    {
        // create the implicit coefficient matrix for the first halfstep
        for (int i=1; i<dim-1; i++)
        {
            a[i] = -ALPHA[j][i];
            c[i] = -ALPHA[j][i];
            b[i] = 2.0*ALPHA[j][i] + 1.0;
        }

        b[0] = 1;
        b[dim-1] = 1;
        a[0] = 0;
        a[dim-1] = 0;
        c[0] = 0;
        c[dim-1] = 0;

        // calculate the explicit solution vector first halfstep
        d[0] = P[j][0];
        d[dim-1] = P[j][dim-1];
        for (int i=1; i<dim-1; i++)
            d[i] = BETA[j][i]*P[j-1][i] + (1.0 - 2.0*BETA[j][i])*P[j][i] + BETA[j][i]*P[j+1][i];

        // now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i=1; i < dim-1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i-1] *a[i]);
            d2[i] = (d[i] - d2[i-1]*a[i]) / (b[i] - c2[i-1]*a[i]);
        }

        // BUFFER[j][dim-1] = d2[dim-1];
        BUFFER[j][dim-1] = P[j][dim-1];
        for (int i=dim-2; i>-1; i--)
        {
            BUFFER[j][i] = d2[i] - c2[i]*BUFFER[j][i+1];
        }
    }

    // second part of the calculation (timestep n+1/2 -> n+1)
    // y-sweep
#pragma omp parallel for shared(BUFFER) private(a,b,c,d,c2,d2)
    for (int j=1; j<dim-1; j++)
    {
        // create the implicit coefficient matrix for the first halfstep
        for (int i=1; i<dim-1; i++)
        {
            a[i] = -BETA[i][j];
            c[i] = -BETA[i][j];
            b[i] = 2.0*BETA[i][j] + 1.0;
        }

        b[0] = 1;
        b[dim-1] = 1;
        a[0] = 0;
        a[dim-1] = 0;
        c[0] = 0;
        c[dim-1] = 0;

        // calculate the explicit solution vector first irst halfstep
        d[0] = P[0][j];
        d[dim-1] = P[dim-1][j];
        for (int i=1; i<dim-1; i++)
            d[i] = ALPHA[i][j]*BUFFER[i][j-1] + (1.0 - 2.0*ALPHA[i][j])*BUFFER[i][j] + ALPHA[i][j]*BUFFER[i][j+1];

        // now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i=1; i < dim-1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i-1] *a[i]);
            d2[i] = (d[i] - d2[i-1]*a[i]) / (b[i] - c2[i-1]*a[i]);
        }

        for (int i=dim-2; i>-1; i--)
        {
            P[i][j] = d2[i] - c2[i]*P[i+1][j];
        }

    }

    // finally update the other stresses we use
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            Nodes[i][j].P_corrected = P[i+1][j+1];
        }
    }

    for (int i=0;i<dimx;i++)
    {
        Nodes[i][0].P_corrected = P[i+1][0];
        Nodes[i][dimx-1].P_corrected = P[i+1][dim-1];
    }

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            Nodes[i][j].P = Nodes[i][j].P_corrected + Nodes[i][j].depth * g * water_density;
            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity)*Nodes[i][j].P_solid - Nodes[i][j].porosity*Nodes[i][j].P;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            Nodes[i][j].head = - Nodes[i][j].depth + (Nodes[i][j].P / (g*water_density) );           
        }
    }
}



//*************************************************************************************************************
// The heterogeneous solution from Irfan and Daniel, see: "Dynamic development of hydrofracture"
//
// Unfortunately this algo has *the same* problems as the other one.
//
// Don't use.
//*************************************************************************************************************

void Model::SolvePressureADIHeterogeneous()
{

    if (dimx != dimy)
    {
        cout << "If you want to use the ADI solver, the system needs to be of square dimensions" << endl;
        exit(0);
    }

    int dim = dimx+2;

    REAL a[dim], b[dim], c[dim], d[dim], c2[dim], d2[dim];
    REAL BUFFER[dim][dim];
    REAL ALPHA[dim][dim];
    REAL BETA[dim][dim];
    REAL P[dim][dim];

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            ALPHA[i+1][j+1] = Nodes[i][j].alpha_het;
            BETA[i+1][j+1] = Nodes[i][j].beta_het;
            P[i+1][j+1] = Nodes[i][j].P_corrected;
        }
    }

    for (int i=0;i<dimx;i++)
    {
        ALPHA[0][i+1] = Nodes[dimx-1][i].alpha_het;
        BETA[0][i+1] = Nodes[dimx-1][i].beta_het;
        P[0][i+1] = Nodes[dimx-1][i].P_corrected;

        ALPHA[dim-1][i+1] = Nodes[0][i].alpha_het;
        BETA[dim-1][i+1] = Nodes[0][i].beta_het;
        P[dim-1][i+1] = Nodes[0][i].P_corrected;

        ALPHA[i+1][0] = Nodes[i][dimx-1].alpha_het;
        BETA[i+1][0] = Nodes[i][dimx-1].beta_het;
        P[i+1][0] = Nodes[i][0].P_corrected;

        ALPHA[i+1][dim-1] = Nodes[i][0].alpha_het;
        BETA[i+1][dim-1] = Nodes[i][0].beta_het;
        P[i+1][dim-1] = Nodes[i][dimx-1].P_corrected;
    }


    // first part of the calculation, x-sweep
#pragma omp parallel for shared(BUFFER) private(a,b,c,d,c2,d2)
    for (int j=1; j<dim-1; j++)
    {
        // create the implicit coefficient matrix for the first halfstep
        for (int i=1; i<dim-1; i++)
        {
            a[i] = -ALPHA[j][i];
            c[i] = -ALPHA[j][i];
            b[i] = 2.0*ALPHA[j][i] + 1.0;
        }

        b[0] = 1;
        b[dim-1] = 1;
        a[0] = 0;
        a[dim-1] = 0;
        c[0] = 0;
        c[dim-1] = 0;

        // calculate the explicit solution vector first halfstep
        d[0] = P[j][0];
        d[dim-1] = P[j][dim-1];
        for (int i=1; i<dim-1; i++)
            d[i] = BETA[j][i]*P[j-1][i] + (1.0 - 2.0*BETA[j][i])*P[j][i] + BETA[j][i]*P[j+1][i];

        // now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i=1; i < dim-1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i-1] *a[i]);
            d2[i] = (d[i] - d2[i-1]*a[i]) / (b[i] - c2[i-1]*a[i]);
        }

        // BUFFER[j][dim-1] = d2[dim-1];
        BUFFER[j][dim-1] = P[j][dim-1];
        for (int i=dim-2; i>-1; i--)
        {
            BUFFER[j][i] = d2[i] - c2[i]*BUFFER[j][i+1];
        }
    }

    // second part of the calculation (timestep n+1/2 -> n+1)
    // y-sweep
#pragma omp parallel for shared(BUFFER) private(a,b,c,d,c2,d2)
    for (int j=1; j<dim-1; j++)
    {
        // create the implicit coefficient matrix for the first halfstep
        for (int i=1; i<dim-1; i++)
        {
            a[i] = -BETA[i][j];
            c[i] = -BETA[i][j];
            b[i] = 2.0*BETA[i][j] + 1.0;
        }

        b[0] = 1;
        b[dim-1] = 1;
        a[0] = 0;
        a[dim-1] = 0;
        c[0] = 0;
        c[dim-1] = 0;

        // calculate the explicit solution vector first irst halfstep
        d[0] = P[0][j];
        d[dim-1] = P[dim-1][j];
        for (int i=1; i<dim-1; i++)
            d[i] = ALPHA[i][j]*BUFFER[i][j-1] + (1.0 - 2.0*ALPHA[i][j])*BUFFER[i][j] + ALPHA[i][j]*BUFFER[i][j+1];

        // now solve the system with the Thomas algorithm
        c2[0] = c[0] / b[0];
        d2[0] = d[0] / b[0];

        for (int i=1; i < dim-1; i++)
        {
            c2[i] = c[i] / (b[i] - c2[i-1] *a[i]);
            d2[i] = (d[i] - d2[i-1]*a[i]) / (b[i] - c2[i-1]*a[i]);
        }

        for (int i=dim-2; i>-1; i--)
        {
            P[i][j] = d2[i] - c2[i]*P[i+1][j];
        }

    }

    // finally update the other stresses we use
    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            Nodes[i][j].P_corrected = P[i+1][j+1];
        }
    }

    for (int i=0;i<dimx;i++)
    {
        Nodes[i][0].P_corrected = P[i+1][0];
        Nodes[i][dimx-1].P_corrected = P[i+1][dim-1];
    }

    for (int i=0; i<dimx; i++)
    {
        for (int j=0; j<dimx; j++)
        {
            Nodes[i][j].P = Nodes[i][j].P_corrected + Nodes[i][j].depth * g * water_density;
            Nodes[i][j].P_total = (1.0 - Nodes[i][j].porosity)*Nodes[i][j].P_solid - Nodes[i][j].porosity*Nodes[i][j].P;
            Nodes[i][j].P_effective = (Nodes[i][j].sx + Nodes[i][j].sy)/2.0 - Nodes[i][j].P;
            Nodes[i][j].head = - Nodes[i][j].depth + (Nodes[i][j].P / (g*water_density) );
        }
    }
}
