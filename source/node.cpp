#include "node.h"
#include <iostream>
#include <math.h>

Node::Node()
{
}

void Node::InitNode(REAL dx, REAL dy, REAL top_layer_depth, REAL dt, int i, int j, int dimx, int dimy)
{
    row_index = j;
    col_index = i;

    top_boundary = false;
    bottom_boundary = false;
    left_boundary = false;
    right_boundary = false;

    if (row_index == dimy-1)
        top_boundary = true;
    if (row_index == 0)
        bottom_boundary = true;
    if (col_index == 0)
        left_boundary = true;
    if (col_index == dimx-1)
        right_boundary = true;

    if (top_boundary || bottom_boundary || left_boundary || right_boundary)
        boundary = true;

	//ignore_fracture = false;

    fracture_number = -1; // -1 := I don't belong to any fracture cluster yet

    preliminary_broken_in_y = false;
    preliminary_broken_in_x = false;
    
	increased_conductivity_and_permeability_in_x = false;
	increased_conductivity_and_permeability_in_y = false;

    dx = dx;
    dy = dy;
    dt = dt;

    fracture = 0.0;

    preserve_pressure = false;

    released_metamorphic_fluid = 0.0;
    max_metamorphic_fluid = 0.05;

    no_break = false;

    real_xpos = dx * REAL(col_index);
    real_ypos = - dy * REAL(dimy - 1 - row_index) - top_layer_depth;

    volume = dx * dy;
    
    damage_zone_x = false;
	damage_zone_y = false;

//    ypos = REAL(row_index) / REAL(dimy);
//    xpos = REAL(col_index) / REAL(dimx);

	young = 1e10;					// Youngs modulus

    ypos = real_ypos / ((dimy-1)*dy);
    xpos = REAL(col_index) / REAL(dimx);

    depth = -real_ypos + dy;
	//~ depth = 0.0;
	
    rock_viscosity =
            rock_viscosity_initial = 1e22; // - (1e21 - 1e17)*depth/12000.0;				// fantasy value for newtonain viscosity
//    if (depth > 4000) rock_viscosity =
//            rock_viscosity_initial = 1e17;				// fantasy value for newtonain viscosity

    rock_compressibility = 3e-10;					// standard value for Granite (wikipedia)
    n_sigma = 3.3;								// Ji and Zhao, 1993, Granite
    D = 2e-6;										// Ji and Zhao, 1993, Granite
    Q = 187.0;									// Ji and Zhao, 1993, Granite

    porosity =
            porosity_initial =
            porosity_last = 0.01;				// Ruhr sandstone (Emmanuel Detournay, Table 4)

    sy = depth * av_crustal_density * g;
    sx = sy; //(0.33 / 1.0 - 0.33) * sy;
    //~ sy = 0.0;
    //~ sx = 0.0;
    sxy = 0;

    P_solid = (sy + sx) / 2.0;
    P = prev_P = P_initial = P_last = depth * g * water_density;

//    if (top_boundary)
//        P = P_initial = P_last = 1e8;

    P_total = (1.0 - porosity) * P_solid + porosity * P;
    P_effective = (sx + sy)/2.0 - P;
    P_corrected = 0.0;

    head = - depth + (P / (g*water_density) );

    temperature = depth / 1000.0 * 40.0;			// assuming the normal thermal gradient here

    tensile_strength_x = initial_tensile_strength_x = -1e6;						// maybe a good standard setting
    tensile_strength_y = initial_tensile_strength_y = -1e6;						// maybe a good standard setting
    
    //if (depth > 2500 && depth < 2800)
    //{
	//	tensile_strength_x = initial_tensile_strength_x = -10e6;
	//	tensile_strength_y = initial_tensile_strength_y = -10e6;
	//}

	permanent_damage_x = false;
	permanent_damage_x = false;

    fluid_viscosity = 0.000315;					// viscosity of water at 90°C (Internet)
    fluid_compressibility = 4.6e-10;				// compressibility of water at 20°C (Wikipedia)

    x_conductivity =
            x_conductivity_initial = 1e-9;		// Ruhr sandstone (Emmanuel Detournay, Table 4)
    y_conductivity =
            y_conductivity_initial = 1e-9;		// Ruhr sandstone (Emmanuel Detournay, Table 4)

    x_permeability =
            x_permeability_initial = x_conductivity * water_viscosity / (water_density * g);	
    y_permeability =
            y_permeability_initial = y_conductivity * water_viscosity / (water_density * g);	

    //if (row_index > 200)
        //y_conductivity = REAL(row_index-200) / REAL(dimy-200) * 5e-2;

    x_aperture =
            y_aperture = 0.0;						// intact material in the beginning!

	x_damage = 
			y_damage = 0.0;							// intact material in the beginning!

    fluid_density = water_density / (1.0 - P/water_bulk_modulus);				// initialized the density

	//~ if (top_boundary)
	//~ {
		//~ broken_in_x = true;
		//~ broken_in_y = true;
		//~ fracture = 1.0;
	//~ }
	//~ else
	{
		broken_in_x = false;
		broken_in_y = false;
	}

    CalculateFDParameters();				//homogeneous ADI solution

    CalculateHeterogeneousFDParameters();	//heterogeneous ADI solution

    fluid_mass = porosity * dx *dy * fluid_density;

    //~ if (depth > 10000)
    //~ {
        //~ porosity =
            //~ porosity_initial =
            //~ porosity_last = 0.21;			// Ruhr sandstone (Emmanuel Detournay, Table 4)
//~ 
        //~ x_permeability =
                //~ x_permeability_initial = 2.0e-7;		// Ruhr sandstone (Emmanuel Detournay, Table 4)
        //~ y_permeability =
                //~ y_permeability_initial = 2.0e-7;				// Ruhr sandstone (Emmanuel Detournay, Table 4)
//~ 
        //~ x_conductivity =
                //~ x_conductivity_initial = 1e-5;		// Ruhr sandstone (Emmanuel Detournay, Table 4)
        //~ y_conductivity =
                //~ y_conductivity_initial = 1e-5;		// Ruhr sandstone (Emmanuel Detournay, Table 4)
//~ 
    //~ }
    
    memory_conductivity_x = 0.0;
    memory_conductivity_y = 0.0;

}

void Node::CalculateDiffusivity()
{
    x_diffusivity = x_permeability / (porosity*fluid_viscosity*fluid_compressibility);
    y_diffusivity = y_permeability / (porosity*fluid_viscosity*fluid_compressibility);

//    x_diffusivity = 1e-4;
//    y_diffusivity = 1e-4;
}

void Node::CalculateFDParameters()
{
    CalculateDiffusivity(); 				//homogeneous ADI solution

    alpha = y_diffusivity*dt / (2.0*dy*dy);
    beta = x_diffusivity*dt / (2.0*dx*dx);

//    if (col_index < 30 || col_index > 70)
//    {
//        beta = 0.0;
//        alpha = 0.0;
//    }

//    if (row_index < 30 || row_index > 70)
//    {
//        beta = 0.0;
//        alpha = 0.0;
//    }
}

void Node::CalculateHeterogeneousFDParameters()
{
    REAL factor = 0.0;

    factor = y_permeability * dt ;
    factor /= (2.0 * fluid_viscosity * water_compressibility_base * porosity * dy*dy);
    alpha_het = (1.0 + water_compressibility_base * P_corrected) * factor;

    factor = x_permeability * dt ;
    factor /= (2.0 * fluid_viscosity * water_compressibility_base * porosity * dx*dx);
    beta_het = (1.0 + water_compressibility_base * P_corrected) * factor;

    if (col_index < 30 || col_index > 70)
    {
        beta_het = 0.0;
        alpha_het = 0.0;
    }

    if (row_index < 30 || row_index > 70)
    {
        beta_het = 0.0;
        alpha_het = 0.0;
    }

}
