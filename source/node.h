#ifndef NODE_H
#define NODE_H

#include "definitions.h"

class Node
{
public:
    Node();

    void InitNode(REAL dx, REAL dy, REAL top_layer_depth, REAL dt, int i, int j, int dimx, int dimy);

    void CalculateDiffusivity();
    void CalculateFDParameters();				// homogeneous ADI solution
    void CalculateHeterogeneousFDParameters();	// heterogeneous ADI solution
	
    // programmers variables
    int col_index;
    int row_index;
	
    bool top_boundary;
    bool bottom_boundary;
    bool left_boundary;
    bool right_boundary;
    bool boundary;

    bool preserve_pressure; // if the node should not be included in the flow routine
    
    //bool ignore_fracture;
	
    bool broken_in_x;
    bool broken_in_y;
    
    bool damage_zone_x;
    bool damage_zone_y;
    
    bool permanent_damage_x;
	bool permanent_damage_y;
        
    REAL memory_conductivity_x;
    REAL memory_conductivity_y;
    
    bool preliminary_broken_in_y;
    bool preliminary_broken_in_x;
    
    bool increased_conductivity_and_permeability_in_x;
    bool increased_conductivity_and_permeability_in_y;
    
    bool no_break;

    int fracture_number;

    REAL ypos;
    REAL xpos;
    
	REAL young;

    // random number
    REAL frac_random_number;

    // physical dimensions
    REAL dx;
    REAL dy;
    REAL dt;

    REAL real_ypos;
    REAL real_xpos;

    REAL depth;

    REAL volume;

    // pressure and temperature
    REAL P;
    REAL prev_P;
    REAL P_last;
    REAL P_initial;
    REAL P_solid;
    REAL P_total;
    REAL P_corrected;	// this is identical to the 'excess' pressure in Wang: Theory of linear poroelasticity
    REAL P_effective;
    REAL sx, sy, sxy;

    int fracture;

    REAL fluid_mass;

    REAL head;

    REAL temperature;	// temperature

    REAL released_metamorphic_fluid;		// in wt percent
    REAL max_metamorphic_fluid;				// in wt percent

    // hydraulic variables
    REAL x_diffusivity;
    REAL y_diffusivity;

    REAL x_permeability;
    REAL y_permeability;
    REAL x_permeability_initial;
    REAL y_permeability_initial;

    REAL x_conductivity;
    REAL y_conductivity;
    REAL x_conductivity_initial;
    REAL y_conductivity_initial;

    REAL x_aperture;	// total aperture in x (all fractures summed up)
    REAL y_aperture;	// total aperture in y (all fractures summed up)

    REAL x_damage;		// 'damage' in x (calculated from aperture and an assumed fracture porosity)
    REAL y_damage;		// 'damage' in y (calculated from aperture and an assumed fracture porosity)

    // fluid properties
    REAL fluid_viscosity;
    REAL fluid_compressibility;
    REAL fluid_density;
    REAL fluid_content;

    // rock properties
    REAL rock_viscosity;			// for newtonian viscosity
    REAL rock_viscosity_initial;
    REAL rock_compressibility;
    REAL porosity;
    REAL porosity_initial;
    REAL porosity_last;
    REAL tensile_strength_x;
    REAL initial_tensile_strength_x;
    REAL tensile_strength_y;
    REAL initial_tensile_strength_y;
    REAL n_sigma;	// exponent
    REAL D;			// prefactor
    REAL Q;			// activation energy

    // coefficients for the ADI solution
    REAL alpha;
    REAL beta;
    REAL alpha_het;
    REAL beta_het;

    // (poro)elastic state variables
    REAL H;		// reciprocal of poroelastic expansion coefficient
    REAL R;		// reciprokal of unconstrained specific storage coefficient (constant stress storage coefficient)
    REAL K;		// drained bulk modulus
    REAL K_u;	// undrained bulk modulus
    REAL K_f;	// fluid compressibility

    // variables for carman-kozeny
};

#endif // NODE_H
