#ifndef STATEVARIABLES_H
#define STATEVARIABLES_H

#include "definitions.h"

class StateVariables
{
public:
    StateVariables();

    REAL fluid_mass;
    REAL fluid_mass_loss;
};

#endif // STATEVARIABLES_H
