#include "statusdialog.h"
#include "ui_statusdialog.h"

StatusDialog::StatusDialog(StateVariables *statevariables, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StatusDialog)
{
    ui->setupUi(this);

    ui->label_fluid_mass->setText(QString::number(statevariables->fluid_mass));
}

StatusDialog::~StatusDialog()
{
    delete ui;
}
