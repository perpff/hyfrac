#ifndef STATUSDIALOG_H
#define STATUSDIALOG_H

#include <QDialog>
#include "statevariables.h"

namespace Ui {
class StatusDialog;
}

class StatusDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StatusDialog(StateVariables *statevariables = 0, QWidget *parent = 0);
    ~StatusDialog();

private:
    Ui::StatusDialog *ui;
};

#endif // STATUSDIALOG_H
