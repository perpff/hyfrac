/********************************************************************************
** Form generated from reading UI file 'basicsettingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BASICSETTINGSDIALOG_H
#define UI_BASICSETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_BasicSettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_scale_bar_min;
    QDoubleSpinBox *doubleSpinBox_scale_bar_max;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_scale_bar_max;
    QDoubleSpinBox *doubleSpinBox_scale_bar_min;
    QFrame *line;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QSlider *horizontalSlider_aperture_min;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_2;
    QSlider *horizontalSlider_aperture_max;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *BasicSettingsDialog)
    {
        if (BasicSettingsDialog->objectName().isEmpty())
            BasicSettingsDialog->setObjectName(QStringLiteral("BasicSettingsDialog"));
        BasicSettingsDialog->setWindowModality(Qt::NonModal);
        BasicSettingsDialog->resize(400, 300);
        BasicSettingsDialog->setSizeGripEnabled(true);
        BasicSettingsDialog->setModal(true);
        verticalLayout = new QVBoxLayout(BasicSettingsDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_scale_bar_min = new QLabel(BasicSettingsDialog);
        label_scale_bar_min->setObjectName(QStringLiteral("label_scale_bar_min"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_scale_bar_min->sizePolicy().hasHeightForWidth());
        label_scale_bar_min->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(label_scale_bar_min);

        doubleSpinBox_scale_bar_max = new QDoubleSpinBox(BasicSettingsDialog);
        doubleSpinBox_scale_bar_max->setObjectName(QStringLiteral("doubleSpinBox_scale_bar_max"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(doubleSpinBox_scale_bar_max->sizePolicy().hasHeightForWidth());
        doubleSpinBox_scale_bar_max->setSizePolicy(sizePolicy1);
        doubleSpinBox_scale_bar_max->setDecimals(3);

        horizontalLayout_3->addWidget(doubleSpinBox_scale_bar_max);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_scale_bar_max = new QLabel(BasicSettingsDialog);
        label_scale_bar_max->setObjectName(QStringLiteral("label_scale_bar_max"));
        sizePolicy.setHeightForWidth(label_scale_bar_max->sizePolicy().hasHeightForWidth());
        label_scale_bar_max->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label_scale_bar_max);

        doubleSpinBox_scale_bar_min = new QDoubleSpinBox(BasicSettingsDialog);
        doubleSpinBox_scale_bar_min->setObjectName(QStringLiteral("doubleSpinBox_scale_bar_min"));
        sizePolicy.setHeightForWidth(doubleSpinBox_scale_bar_min->sizePolicy().hasHeightForWidth());
        doubleSpinBox_scale_bar_min->setSizePolicy(sizePolicy);
        doubleSpinBox_scale_bar_min->setDecimals(3);

        horizontalLayout_2->addWidget(doubleSpinBox_scale_bar_min);


        verticalLayout_2->addLayout(horizontalLayout_2);


        verticalLayout->addLayout(verticalLayout_2);

        line = new QFrame(BasicSettingsDialog);
        line->setObjectName(QStringLiteral("line"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(line->sizePolicy().hasHeightForWidth());
        line->setSizePolicy(sizePolicy2);
        line->setFrameShadow(QFrame::Sunken);
        line->setLineWidth(1);
        line->setMidLineWidth(0);
        line->setFrameShape(QFrame::HLine);

        verticalLayout->addWidget(line);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(BasicSettingsDialog);
        label->setObjectName(QStringLiteral("label"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy3);

        horizontalLayout_4->addWidget(label);

        horizontalSlider_aperture_min = new QSlider(BasicSettingsDialog);
        horizontalSlider_aperture_min->setObjectName(QStringLiteral("horizontalSlider_aperture_min"));
        sizePolicy2.setHeightForWidth(horizontalSlider_aperture_min->sizePolicy().hasHeightForWidth());
        horizontalSlider_aperture_min->setSizePolicy(sizePolicy2);
        horizontalSlider_aperture_min->setMaximum(100);
        horizontalSlider_aperture_min->setValue(0);
        horizontalSlider_aperture_min->setSliderPosition(0);
        horizontalSlider_aperture_min->setTracking(false);
        horizontalSlider_aperture_min->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(horizontalSlider_aperture_min);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_2 = new QLabel(BasicSettingsDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        sizePolicy3.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy3);

        horizontalLayout_5->addWidget(label_2);

        horizontalSlider_aperture_max = new QSlider(BasicSettingsDialog);
        horizontalSlider_aperture_max->setObjectName(QStringLiteral("horizontalSlider_aperture_max"));
        horizontalSlider_aperture_max->setMaximum(100);
        horizontalSlider_aperture_max->setValue(100);
        horizontalSlider_aperture_max->setTracking(false);
        horizontalSlider_aperture_max->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(horizontalSlider_aperture_max);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_2 = new QPushButton(BasicSettingsDialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(BasicSettingsDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        buttonBox = new QDialogButtonBox(BasicSettingsDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout->addWidget(buttonBox);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(BasicSettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), BasicSettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), BasicSettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(BasicSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *BasicSettingsDialog)
    {
        BasicSettingsDialog->setWindowTitle(QApplication::translate("BasicSettingsDialog", "Settings", 0));
        label_scale_bar_min->setText(QApplication::translate("BasicSettingsDialog", "Scale bar maximum", 0));
        label_scale_bar_max->setText(QApplication::translate("BasicSettingsDialog", "Scale bar minimum", 0));
        label->setText(QApplication::translate("BasicSettingsDialog", "\303\201perture minimum", 0));
        label_2->setText(QApplication::translate("BasicSettingsDialog", "Aperture maximum", 0));
        pushButton_2->setText(QApplication::translate("BasicSettingsDialog", "Reload", 0));
        pushButton->setText(QApplication::translate("BasicSettingsDialog", "Apply", 0));
    } // retranslateUi

};

namespace Ui {
    class BasicSettingsDialog: public Ui_BasicSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BASICSETTINGSDIALOG_H
