/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QCustomPlot *customPlot;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_timestep;
    QLabel *label_timestep_number;
    QSpacerItem *horizontalSpacer;
    QLabel *label;
    QLabel *label_time_passed;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_start;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_save_interval;
    QSpinBox *spinBox_save_interval;
    QCheckBox *checkBox_save_png;
    QCheckBox *checkBox_count_fractures;
    QCheckBox *checkBox_continuous_signals;
    QHBoxLayout *horizontalLayout;
    QComboBox *comboBox;
    QPushButton *pushButton_reset_colormap;
    QCheckBox *checkBox_autoscale;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_basic_settings;
    QPushButton *pushButton_state;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_open_previous;
    QPushButton *pushButton_open;
    QPushButton *pushButton_open_next;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_make_movie;
    QPushButton *pushButton_save_png;
    QPushButton *pushButton_save_model;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(700, 735);
        MainWindow->setMinimumSize(QSize(531, 0));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        customPlot = new QCustomPlot(centralWidget);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        sizePolicy.setHeightForWidth(customPlot->sizePolicy().hasHeightForWidth());
        customPlot->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(customPlot);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_timestep = new QLabel(centralWidget);
        label_timestep->setObjectName(QStringLiteral("label_timestep"));

        horizontalLayout_4->addWidget(label_timestep);

        label_timestep_number = new QLabel(centralWidget);
        label_timestep_number->setObjectName(QStringLiteral("label_timestep_number"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_timestep_number->sizePolicy().hasHeightForWidth());
        label_timestep_number->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_timestep_number);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        label_time_passed = new QLabel(centralWidget);
        label_time_passed->setObjectName(QStringLiteral("label_time_passed"));
        sizePolicy1.setHeightForWidth(label_time_passed->sizePolicy().hasHeightForWidth());
        label_time_passed->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(label_time_passed);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        pushButton_start = new QPushButton(centralWidget);
        pushButton_start->setObjectName(QStringLiteral("pushButton_start"));

        horizontalLayout_2->addWidget(pushButton_start);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        label_save_interval = new QLabel(centralWidget);
        label_save_interval->setObjectName(QStringLiteral("label_save_interval"));

        horizontalLayout_2->addWidget(label_save_interval);

        spinBox_save_interval = new QSpinBox(centralWidget);
        spinBox_save_interval->setObjectName(QStringLiteral("spinBox_save_interval"));
        spinBox_save_interval->setButtonSymbols(QAbstractSpinBox::PlusMinus);
        spinBox_save_interval->setMaximum(100001);
        spinBox_save_interval->setValue(10);

        horizontalLayout_2->addWidget(spinBox_save_interval);

        checkBox_save_png = new QCheckBox(centralWidget);
        checkBox_save_png->setObjectName(QStringLiteral("checkBox_save_png"));

        horizontalLayout_2->addWidget(checkBox_save_png);

        checkBox_count_fractures = new QCheckBox(centralWidget);
        checkBox_count_fractures->setObjectName(QStringLiteral("checkBox_count_fractures"));
        checkBox_count_fractures->setToolTipDuration(10000);
        checkBox_count_fractures->setTristate(false);

        horizontalLayout_2->addWidget(checkBox_count_fractures);

        checkBox_continuous_signals = new QCheckBox(centralWidget);
        checkBox_continuous_signals->setObjectName(QStringLiteral("checkBox_continuous_signals"));

        horizontalLayout_2->addWidget(checkBox_continuous_signals);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setCurrentText(QStringLiteral("Fractures"));

        horizontalLayout->addWidget(comboBox);

        pushButton_reset_colormap = new QPushButton(centralWidget);
        pushButton_reset_colormap->setObjectName(QStringLiteral("pushButton_reset_colormap"));

        horizontalLayout->addWidget(pushButton_reset_colormap);

        checkBox_autoscale = new QCheckBox(centralWidget);
        checkBox_autoscale->setObjectName(QStringLiteral("checkBox_autoscale"));
        checkBox_autoscale->setChecked(false);

        horizontalLayout->addWidget(checkBox_autoscale);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        pushButton_basic_settings = new QPushButton(centralWidget);
        pushButton_basic_settings->setObjectName(QStringLiteral("pushButton_basic_settings"));
        pushButton_basic_settings->setFlat(false);

        horizontalLayout->addWidget(pushButton_basic_settings);

        pushButton_state = new QPushButton(centralWidget);
        pushButton_state->setObjectName(QStringLiteral("pushButton_state"));

        horizontalLayout->addWidget(pushButton_state);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        pushButton_open_previous = new QPushButton(centralWidget);
        pushButton_open_previous->setObjectName(QStringLiteral("pushButton_open_previous"));

        horizontalLayout_3->addWidget(pushButton_open_previous);

        pushButton_open = new QPushButton(centralWidget);
        pushButton_open->setObjectName(QStringLiteral("pushButton_open"));

        horizontalLayout_3->addWidget(pushButton_open);

        pushButton_open_next = new QPushButton(centralWidget);
        pushButton_open_next->setObjectName(QStringLiteral("pushButton_open_next"));

        horizontalLayout_3->addWidget(pushButton_open_next);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        pushButton_make_movie = new QPushButton(centralWidget);
        pushButton_make_movie->setObjectName(QStringLiteral("pushButton_make_movie"));

        horizontalLayout_3->addWidget(pushButton_make_movie);

        pushButton_save_png = new QPushButton(centralWidget);
        pushButton_save_png->setObjectName(QStringLiteral("pushButton_save_png"));

        horizontalLayout_3->addWidget(pushButton_save_png);

        pushButton_save_model = new QPushButton(centralWidget);
        pushButton_save_model->setObjectName(QStringLiteral("pushButton_save_model"));

        horizontalLayout_3->addWidget(pushButton_save_model);


        verticalLayout->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "QCustomPlot plot examples", 0));
        label_timestep->setText(QApplication::translate("MainWindow", "Timestep:", 0));
        label_timestep_number->setText(QApplication::translate("MainWindow", "0", 0));
        label->setText(QApplication::translate("MainWindow", "Time passed (a): ", 0));
        label_time_passed->setText(QApplication::translate("MainWindow", "0", 0));
        pushButton_start->setText(QApplication::translate("MainWindow", "Run", 0));
        label_save_interval->setText(QApplication::translate("MainWindow", "Save interval:", 0));
#ifndef QT_NO_TOOLTIP
        checkBox_save_png->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>Save also a png file, whenever you save the model.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        checkBox_save_png->setText(QApplication::translate("MainWindow", "png", 0));
#ifndef QT_NO_TOOLTIP
        checkBox_count_fractures->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>If this is checked the model will be saved after <span style=\" font-weight:600;\">every</span> timestep and also after every <span style=\" font-weight:600;\">nth fracture</span>.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        checkBox_count_fractures->setText(QApplication::translate("MainWindow", "Count fractures", 0));
#ifndef QT_NO_TOOLTIP
        checkBox_continuous_signals->setToolTip(QApplication::translate("MainWindow", "<html><head/><body><p>If checked, the signal-files are updated whenever a new fracture forms <span style=\" font-weight:600;\">and</span> at the end of every timestep.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        checkBox_continuous_signals->setText(QApplication::translate("MainWindow", "Continuous signals", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Fractures", 0)
         << QApplication::translate("MainWindow", "Broken in X", 0)
         << QApplication::translate("MainWindow", "Broken in Y", 0)
         << QApplication::translate("MainWindow", "xaperture/spacing", 0)
         << QApplication::translate("MainWindow", "yaperture/spacing", 0)
         << QApplication::translate("MainWindow", "Conductivity in X", 0)
         << QApplication::translate("MainWindow", "Conductivity in Y", 0)
         << QApplication::translate("MainWindow", "Active fractures", 0)
         << QApplication::translate("MainWindow", "Inactive fractures", 0)
         << QApplication::translate("MainWindow", "Hydraulic head", 0)
         << QApplication::translate("MainWindow", "Fluid pressure", 0)
         << QApplication::translate("MainWindow", "Total pressure", 0)
         << QApplication::translate("MainWindow", "Effective pressure", 0)
         << QApplication::translate("MainWindow", "x-stress", 0)
         << QApplication::translate("MainWindow", "y-stress", 0)
         << QApplication::translate("MainWindow", "Viscosity", 0)
         << QApplication::translate("MainWindow", "Porosity", 0)
         << QApplication::translate("MainWindow", "Temperature", 0)
         << QApplication::translate("MainWindow", "-------------------------------", 0)
         << QApplication::translate("MainWindow", "Tensile strength X", 0)
         << QApplication::translate("MainWindow", "Tensile strength Y", 0)
         << QApplication::translate("MainWindow", "Fracture number", 0)
         << QApplication::translate("MainWindow", "x_damage", 0)
         << QApplication::translate("MainWindow", "y_damage", 0)
         << QApplication::translate("MainWindow", "Diffusivity in X", 0)
         << QApplication::translate("MainWindow", "Diffusivity in Y", 0)
         << QApplication::translate("MainWindow", "Solid pressure", 0)
         << QApplication::translate("MainWindow", "Corrected fluid pressure", 0)
        );
        pushButton_reset_colormap->setText(QApplication::translate("MainWindow", "Reset scale", 0));
        checkBox_autoscale->setText(QApplication::translate("MainWindow", "Autoscale", 0));
        pushButton_basic_settings->setText(QApplication::translate("MainWindow", "Settings", 0));
        pushButton_state->setText(QApplication::translate("MainWindow", "State", 0));
        pushButton_open_previous->setText(QApplication::translate("MainWindow", "Prev", 0));
        pushButton_open->setText(QApplication::translate("MainWindow", "Open", 0));
        pushButton_open_next->setText(QApplication::translate("MainWindow", "Next", 0));
        pushButton_make_movie->setText(QApplication::translate("MainWindow", "Movie", 0));
        pushButton_save_png->setText(QApplication::translate("MainWindow", "Save png", 0));
        pushButton_save_model->setText(QApplication::translate("MainWindow", "Save mod", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
