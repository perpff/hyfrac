/********************************************************************************
** Form generated from reading UI file 'statusdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STATUSDIALOG_H
#define UI_STATUSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_StatusDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *label_fluid_mass;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *StatusDialog)
    {
        if (StatusDialog->objectName().isEmpty())
            StatusDialog->setObjectName(QStringLiteral("StatusDialog"));
        StatusDialog->resize(400, 300);
        StatusDialog->setSizeGripEnabled(true);
        StatusDialog->setModal(true);
        verticalLayout = new QVBoxLayout(StatusDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(StatusDialog);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        label_fluid_mass = new QLabel(StatusDialog);
        label_fluid_mass->setObjectName(QStringLiteral("label_fluid_mass"));

        horizontalLayout->addWidget(label_fluid_mass);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        buttonBox = new QDialogButtonBox(StatusDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        horizontalLayout_2->addWidget(buttonBox);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(StatusDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), StatusDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), StatusDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(StatusDialog);
    } // setupUi

    void retranslateUi(QDialog *StatusDialog)
    {
        StatusDialog->setWindowTitle(QApplication::translate("StatusDialog", "State variables", 0));
        label->setText(QApplication::translate("StatusDialog", "Fluid mass (kg):", 0));
        label_fluid_mass->setText(QApplication::translate("StatusDialog", "0", 0));
    } // retranslateUi

};

namespace Ui {
    class StatusDialog: public Ui_StatusDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STATUSDIALOG_H
