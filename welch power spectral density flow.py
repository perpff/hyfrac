#!/usr/bin/python

# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 12:13:55 2015

@author: till
"""

from scipy import signal
from pylab import *

data = np.loadtxt("flow.csv",  skiprows=1)
t,z = data[:,0], data[:,4]

z = -z

plot(z)
show(block=True)
#raw_input("Press Enter to continue...")

f, Pxx_den = signal.welch(z)
#semilogy(f, Pxx_den)
#plot (f, Pxx_den)
loglog(f, Pxx_den, '.')

"""
fit data to straight line
"""
fitx = log(f[1:])
fity = log(Pxx_den[1:])

p = polyfit(fitx, fity, 1)

plot(f, (exp(p[1])*f**p[0]), 'r-', linewidth=2)

print p[0]

xlabel('frequency [Hz]')
ylabel('PSD [Signal**2/Hz]')
show(block=True)

#raw_input("Press Enter to continue...")
